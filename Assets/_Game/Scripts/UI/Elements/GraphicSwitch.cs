﻿using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.UI.Elements
{
    [RequireComponent(typeof(Button))]
    public class GraphicSwitch : Switch
    {
        [SerializeField] private Image _TargetChangedImage = null;
        [SerializeField] private Sprite _OnImage = null;
        [SerializeField] private Sprite _OffImage = null;
        
        protected override void UpdateSwitch()
        {
            
            if (_TargetChangedImage == null)
                return;

            _TargetChangedImage.sprite = isOn ? _OnImage : _OffImage;
        }
    }
}