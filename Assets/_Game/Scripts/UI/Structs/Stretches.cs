﻿using System;
using UnityEngine;

namespace _Game.Scripts.UI.Structs
{
    [Serializable]
    struct Stretches
    {
        [SerializeField] private float _Left;
        [SerializeField] private float _Top;
        [SerializeField] private float _Right;
        [SerializeField] private float _Bottom;

        public float left => _Left;
        public float top => _Top;
        public float right => _Right;
        public float bottom => _Bottom;

        public override string ToString() =>
            $"l: {_Left}; b: {_Bottom}; r: {_Right}; t: {_Top}";


        public Stretches(float left, float right, float top, float bottom)
        {
            _Left = left;
            _Right = right;
            _Top = top;
            _Bottom = bottom;
        }
    }
}
