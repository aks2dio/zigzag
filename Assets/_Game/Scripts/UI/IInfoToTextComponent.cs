﻿
namespace _Game.Scripts.UI
{
    public interface IInfoToTextComponent<in T>
    {
        void UpdateTextComponent(T value);
        void UpdateTextComponent<TM>(TM value);
    }
}
