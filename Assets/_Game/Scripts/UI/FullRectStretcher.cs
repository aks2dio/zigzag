﻿using _Game.Scripts.UI.Structs;
using UnityEngine;

namespace _Game.Scripts.UI
{
    class FullRectStretcher : MonoBehaviour
    {
        [SerializeField] private Stretches[] _Stretches = new Stretches[0];

        /////////////////////////////////////////////////////////

        public void SelectSize(int index)
        {
            if (index < 0 || index >= _Stretches.Length)
                return;

            ApplySize(_Stretches[index]);
        }

        ////////////////////////////////////////////////////////

        private void ApplySize(Stretches stretch)
        {
            if (TryGetComponent<RectTransform>(out var rect) == false)
                return;

            rect.offsetMin = new Vector2(stretch.left, stretch.bottom);
            rect.offsetMax = new Vector2(stretch.right, stretch.top);

            UnityEngine.Debug.Log($"Applyed: {stretch}");
        }
    }
}
