﻿using Core.Components;
using Core.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Game.Scripts.UI.Reward
{
    class RewardViewBehaviour : MyMonoBehaviour
    {
        [SerializeField] private CompositeText _Score;
        [SerializeField] private Button _Button;

        private UnityAction onClickActionCache;
        
        //////////////////////////////////////////////
        
        protected override void OnChecking()
        {
            CheckField(_Score);
            CheckField(_Button);
        }

        private void OnDisable()
        {
            _Button.onClick.RemoveListener(onClickActionCache);
            onClickActionCache = null;
        }

        //////////////////////////////////////////////

        public void Init(int score, UnityAction onClick)
        {
            SetScore(score);
            
            onClickActionCache = onClick;
            _Button.onClick.AddListener(onClick);
        }
        
        public void SetScore(int value) => _Score.SetPart(value, 1);
    }
}