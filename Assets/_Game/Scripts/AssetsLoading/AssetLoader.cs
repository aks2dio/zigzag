﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace _Game.Scripts.AssetsLoading
{
    public interface IUnloadable
    {
        void Unload();
    }

    public abstract class AssetLoader<T>: IUnloadable where T : class
    {
        protected T _asset;
        protected Task<T> _currentTask;
        protected AsyncOperationHandle<T>? _handler;

        protected readonly List<Action<T>> _outsideActions = new List<Action<T>>();

        public event Action loading;
        public event Action<T> loaded;

        //////////////////////////////////////

        protected Task<T> StartLoading() => StartLoading(GetLoadingHandle());
        protected Task<T> StartLoading(AsyncOperationHandle handler) => StartLoading(handler.Convert<T>());
        protected Task<T> StartLoading(AsyncOperationHandle<T> handler)
        {
            loading?.Invoke();

            _handler = handler;
            _handler.Value.Completed += OnHandlerCompleted;

            _currentTask = _handler.Value.Task;
            return _currentTask;
        }

        protected abstract AsyncOperationHandle<T> GetLoadingHandle();

        public void Unload()
        {
            _asset = default;
            _currentTask = default;

            OnUnload();

            if (_handler.HasValue && _handler.Value.IsValid())
            {
                _handler.Value.Completed -= OnHandlerCompleted;
                OnHandlerUnload(_handler.Value);
                _handler = null;
            }

            _outsideActions.Clear();
        }

        protected virtual void OnUnload() { }
        protected virtual void OnHandlerUnload(AsyncOperationHandle<T> handler){ }

        //////////////////////////////////////

        protected void OnHandlerCompleted(AsyncOperationHandle handler) =>
            OnHandlerCompleted(handler.Convert<T>());

        protected void OnHandlerCompleted(AsyncOperationHandle<T> handler)
        {
            if (handler.Status != AsyncOperationStatus.Succeeded)
                return;

            OnHandlerCompleted(handler.Result);
        }

        protected void OnHandlerCompleted(T asset)
        {
            if (asset == null)
                return;

            _asset = asset;
            loaded?.Invoke(_asset);

            for (var i = 0; i < _outsideActions.Count; i++)
                _outsideActions[i]?.Invoke(_asset);
        }
    }
}
