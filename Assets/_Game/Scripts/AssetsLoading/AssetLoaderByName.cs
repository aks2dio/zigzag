﻿using System;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace _Game.Scripts.AssetsLoading
{
    public class AssetLoaderByName<T> : AssetLoader<T> where T : class
    {
        private string _assetName;

        //////////////////////////////////////

        public Task<T> Load(string assetName, Action<T> onLoaded = null)
        {
            Unload();
            _assetName = assetName;

            // если Reference пустой, то не пытаемся загружать
            if (string.IsNullOrEmpty(_assetName))
                return Task.FromResult<T>(default);

            // если ассет уже закэширован, то возвращаем его
            if (_asset != null)
            {
                onLoaded?.Invoke(_asset);
                return Task.FromResult(_asset);
            }

            if (onLoaded != null)
                _outsideActions.Add(onLoaded);

            // если процесс загрузки уже начат, то используем его
            if (_currentTask != null)
                return _currentTask;

            // иначе наичаем загрузку самостоятельно
            return StartLoading();
        }

        //////////////////////////////////////

        protected override AsyncOperationHandle<T> GetLoadingHandle() =>
            Addressables.LoadAssetAsync<T>(_assetName);

        protected override void OnHandlerUnload(AsyncOperationHandle<T> handler) =>
            Addressables.Release(handler);
    }
}
