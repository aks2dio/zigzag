﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace _Game.Scripts.AssetsLoading
{
    public class AssetLoaderToImage : IUnloadable
    {
        private static readonly Color _INACTIVE_COLOR = new Color(0, 0, 0, 0);
        private static readonly Color _DEFAULT_COLOR = new Color(1, 1, 1, 1);

        private readonly Image _imageComponent;
        private readonly AssetLoaderByReference<Sprite> _spriteLoader;

        //////////////////////////////////////

        public AssetLoaderToImage(Image imageComponent)
        {
            _imageComponent = imageComponent;
            _spriteLoader = new AssetLoaderByReference<Sprite>();
        }

        //////////////////////////////////////

        public void Load(AssetReferenceSprite imageReference) =>
            Load(imageReference, _DEFAULT_COLOR);

        public async void Load(AssetReferenceSprite imageReference, Color color)
        {
            _imageComponent.color = _INACTIVE_COLOR;

            var image = await _spriteLoader.Load(imageReference);
            _imageComponent.sprite = image;

            _imageComponent.color = color;
        }

        //////////////////////////////////////

        public void Unload()
        {
            _imageComponent.sprite = null;
            _spriteLoader.Unload();
        }
    }
}
