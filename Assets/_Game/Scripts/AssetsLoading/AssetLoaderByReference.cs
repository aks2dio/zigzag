﻿using System;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace _Game.Scripts.AssetsLoading
{
    public class AssetLoaderByReference<T> : AssetLoader<T> where T : class
    {
        private AssetReference _reference;

        //////////////////////////////////////

        public Task<T> Load(AssetReference assetReference, Action<T> onLoaded = null)
        {
            Unload();
            _reference = assetReference;

            // если Reference пустой, то не пытаемся загружать
            if (_reference == null || !_reference.RuntimeKeyIsValid())
                return Task.FromResult<T>(default);

            // если ассет уже загружен в память, то кэшируем его
            if (_asset == null && _reference.IsDone && _reference.Asset)
                _asset = _reference.Asset as T;

            // если ассет уже закэширован, то возвращаем его
            if (_asset != null)
            {
                onLoaded?.Invoke(_asset);
                return Task.FromResult(_asset);
            }

            if (onLoaded != null)
                _outsideActions.Add(onLoaded);

            // если процесс загрузки уже начат, то используем его
            if (_currentTask != null)
                return _currentTask;

            // если где-то уже по этому Reference происходит загрузка, то используем её
            if (_reference.IsValid() && _reference.IsDone == false)
                return StartLoading(_reference.OperationHandle);

            // иначе наичаем загрузку самостоятельно
            return StartLoading();
        }

        //////////////////////////////////////

        protected override AsyncOperationHandle<T> GetLoadingHandle() =>
            _reference.LoadAssetAsync<T>();

        protected override void OnUnload()
        {
            _reference?.ReleaseAsset();
            _reference = default;
        }
    }
}
