﻿using _Game.Scripts.Game.Objects.Gem;
using _Game.Scripts.Game.Objects.Generating.Gem.Factory;
using _Game.Scripts.Game.Objects.Generating.Gem.Pool;
using _Game.Scripts.Game.Objects.Generating.Gem.Positioning;
using _Game.Scripts.Game.Objects.Generating.Gem.Producer;
using _Game.Scripts.Game.Objects.Generating.Gem.Variator;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    class GemManagerInstaller : MonoInstaller<GemManagerInstaller>
    {
        [SerializeField] private Transform _PoolContainer = null;

        [Inject] private IGamePrefabs _gamePrefabs { get; set; }
        [Inject] private IGameParameters _gameParameters { get; set; }

        private bool _USE_POOL = true;


        public override void InstallBindings()
        {
            BindProducer(_USE_POOL);

            Container.Bind<IVariator>().To<IVariator>().FromInstance(_variator).AsTransient();
            Container.Bind<IPositionNoiser>().To<IPositionNoiser>()
                .FromInstance(_positionNoiser).AsSingle();
        }

        private IPositionNoiser _positionNoiser =>
            new InSquarePositionNoiser(
                _gameParameters.GemManager.squareSideOfGenerationArea,
                _gameParameters.GemManager.positionOffsetMask);

        private IVariator _variator {
            get {
                switch (_gameParameters.GemManager.placementType) {
                    case Parameters.Classes.PlacementType.Iterative: return _iterateVariator;
                    case Parameters.Classes.PlacementType.Random: return _randomVariator;
                    default: return _randomVariator;
                }
            }
        }

        private IterativeVariator _iterateVariator
            => new IterativeVariator(_gameParameters.GemManager.platformsInBlock, 1);

        private RandomVariator _randomVariator
            => new RandomVariator(_gameParameters.GemManager.platformsInBlock);

        private void BindProducer(bool usePool)
        {
            var prefab = _gamePrefabs.gem;

            if (usePool)
                BindProducerByPool(prefab);
            else
                BindProducerByFactory(prefab);
        }

        private void BindProducerByFactory(GameObject prefab)
        {
            Container.BindFactory<IGem, GemFactory>().FromComponentInNewPrefab(prefab).AsSingle();
            Container.Bind<IGemFactory>().To<GemFactory>().FromResolve().AsTransient();

            Container.Bind<IGemProducer>().To<GemProducer>().AsTransient();
        }

        private void BindProducerByPool(GameObject prefab)
        {
            var poolSize = 5;

            Container.Bind<Transform>().FromInstance(_PoolContainer).WhenInjectedInto<GemPool>();
            Container.BindMemoryPool<GemObject, GemPool>().WithInitialSize(poolSize)
                .FromComponentInNewPrefab(prefab).UnderTransform(_PoolContainer);

            Container.Bind<IGemPool>().To<GemPool>().FromResolve();
            Container.Bind<IGemProducer>().To<GemProducerByPool>().AsTransient();
        }
    }
}
