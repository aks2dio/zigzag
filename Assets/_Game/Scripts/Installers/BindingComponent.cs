﻿using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Vibration;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    public class BindingComponent : LoggedMonoBehaviour
    {
        [Inject] private SettingsData _SettingsData { get; }
        [Inject] private VibrationManager _VibrationManager { get; }
        
        ////////////////////////////////////////////
        
        private void Start()
        {
            UpdateVibration();
            _SettingsData.onVibrationChanged += UpdateVibration;
        }
        
        ////////////////////////////////////////////

        private void UpdateVibration() =>
            _VibrationManager.enabled = _SettingsData.vibration;
    }
}