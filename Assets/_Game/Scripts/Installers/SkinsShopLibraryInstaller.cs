using _Game.Scripts.Game;
using _Game.Scripts.Game.SkinsShop.Library;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    [CreateAssetMenu(fileName = nameof(SkinsShopLibraryInstaller), menuName = DefinitionsHelper.Installers + nameof(SkinsShopLibraryInstaller))]
    public class SkinsShopLibraryInstaller : ScriptableObjectInstaller<SkinsShopLibraryInstaller>
    {
        [SerializeField] private SkinsShopLibrary _Library = null;

        ////////////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ISkinsShopLibrary>().FromInstance(_Library).AsSingle();
        }
    }
}