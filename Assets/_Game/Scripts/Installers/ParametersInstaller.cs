using _Game.Scripts.Game;
using _Game.Scripts.Game.Input;
using _Game.Scripts.Game.Vibration;
using _Game.Scripts.Parameters.Classes;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    [CreateAssetMenu(fileName = nameof(ParametersInstaller), menuName = DefinitionsHelper.Installers + nameof(ParametersInstaller))]
    public class ParametersInstaller : ScriptableObjectInstaller<ParametersInstaller>
    {
        [SerializeField] private GameParameters _GameParameters = null;
        [SerializeField] private GamePrefabs _GamePrefabs = null;
        [SerializeField] private ControlConfig _ControlConfig = null;
        [SerializeField] private VibrationSettings _VibrationSettings = null;

        public override void InstallBindings()
        {
            Container.Bind<VibrationSettings>().FromInstance(_VibrationSettings).AsSingle();
            Container.Bind<ControlConfig>().FromInstance(_ControlConfig).AsSingle();
            BindInterfaceFromInstance<IGameParameters>(_GameParameters);
            BindInterfaceFromInstance<IGamePrefabs>(_GamePrefabs);

            BindInterfaceFromInstance(_GameParameters.Ball);
            BindInterfaceFromInstance(_GameParameters.Direction);
            BindInterfaceFromInstance(_GameParameters.GemManager);
            BindInterfaceFromInstance(_GameParameters.PlatformManager);
            BindInterfaceFromInstance(_GameParameters.Platform);
            BindInterfaceFromInstance(_GameParameters.GemScore);
            BindInterfaceFromInstance(_GameParameters.TurnsScore);
            BindInterfaceFromInstance(_GameParameters.DistanceScore);
        }

        private void BindInterfaceFromInstance<T>(T instance)
            => Container.BindInterfacesAndSelfTo<T>().FromInstance(instance).AsSingle();
    }
}