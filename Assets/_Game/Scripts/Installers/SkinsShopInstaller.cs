﻿using _Game.Scripts.Game.SkinsShop.View;
using _Game.Scripts.Game.SkinsShop.View.Group;
using _Game.Scripts.Game.SkinsShop.View.Header;
using _Game.Scripts.Game.SkinsShop.View.Item;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Installers
{
    class SkinsShopInstaller : MonoInstaller<SkinsShopInstaller>
    {
        [SerializeField] private SkinsShopItemGroupView _ItemContainerView = null;
        [SerializeField] private SkinsShopHeaderView _HeaderView = null;
        [SerializeField] private SkinsShopItemView _ItemView = null;

        [SerializeField] private Transform _Container = null;

        private void CheckFields()
        {
            CheckField(_ItemContainerView);
            CheckField(_HeaderView);
            CheckField(_ItemView);
            CheckField(_Container);
        }

        private void CheckField<T>(T component) where T : Component
        {
            if (component == null)
                throw new System.NullReferenceException("Null component");
        }

        public override void InstallBindings()
        {
            CheckFields();

            Container.Bind<Transform>().FromInstance(_Container).AsSingle();

            Container.BindFactory<ISkinsShopItemGroupView, SkinsShopItemGroupViewFactory>()
                .FromComponentInNewPrefab(_ItemContainerView).AsSingle();

            Container.Bind<ISkinsShopItemGroupViewFactory>()
                .To<SkinsShopItemGroupViewFactory>().FromResolve().AsTransient();


            Container.BindFactory<ISkinsShopHeaderView, SkinsShopHeaderViewFactory>()
                .FromComponentInNewPrefab(_HeaderView).AsSingle();

            Container.Bind<ISkinsShopHeaderViewFactory>()
                .To<SkinsShopHeaderViewFactory>().FromResolve().AsTransient();


            Container.BindFactory<ISkinsShopItemView, SkinsShopItemViewFactory>()
                .FromComponentInNewPrefab(_ItemView).AsSingle();

            Container.Bind<ISkinsShopItemViewFactory>()
                .To<SkinsShopItemViewFactory>().FromResolve().AsTransient();

            Container.Bind<ISkinsShopLibraryView>().To<SkinsShopLibraryView>().AsTransient();
        }
    }
}
