﻿using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.Instruments
{
    public class Unique : MonoBehaviour
    {
        [SerializeField] private string _UniquenessTag = "";

        public string uniquenessTag => _UniquenessTag;

        private bool _destroyed { get; set; }

        private static List<string> s_uniquenessTags = new List<string>();
        
        /////////////////////////////////////////////////////////////////

        #region MONO_BEHAVIOUR

        private void Awake()
        {
            DeleteIfNotUnique();
            AddTag(uniquenessTag);
        }

        private void OnDestroy()
        {
            RemoveTag(uniquenessTag);
        }

        #endregion // MONO_BEHAVIOUR

        #region PUBLIC_METHODS
        public void SetTag(string tag)
        {
            RemoveTag(_UniquenessTag);
            _UniquenessTag = tag;
            DeleteIfNotUnique();
            AddTag(tag);
        }
        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS
        private void DeleteIfNotUnique()
        {
            if (s_uniquenessTags.Contains(uniquenessTag) == false)
                return;
            
            _destroyed = true;
            DestroyImmediate(gameObject);
        }

        private void AddTag(string tag)
        {
            if (_destroyed || s_uniquenessTags.Contains(tag))
                return;
            
            s_uniquenessTags.Add(tag);
        }

        private void RemoveTag(string tag)
        {
            if (_destroyed || s_uniquenessTags.Contains(tag) == false)
                return;

            s_uniquenessTags.Remove(tag);
        }
        #endregion // PRIVATE_METHODS
    }
}