﻿using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Instruments.Bordering
{
    interface IBorderChecker
    {
        Transform checkedObject { get; }
        Vector3 bordersByCoordinates { get; }
        Vector3Int maskByCoordinates { get; }
        Vector3Int directionByCoordinates { get; }

        event UnityAction onBorderIn;
        event UnityAction onBorderOut;

        void SetCheckedObject(Transform checkedObject);
        void SetBorders(Vector3 bordersByCoordinates);
        void SetDirection(Vector3Int directionByCoordinates);
        void SetMask(Vector3Int maskByCoordinates);
    }
}
