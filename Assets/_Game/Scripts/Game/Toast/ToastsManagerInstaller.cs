﻿using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Toast
{
    public class ToastsManagerInstaller : MonoInstaller<ToastsManagerInstaller>
    {
        [SerializeField] private Canvas _Canvas = null;
        [SerializeField] private Transform _ItemsContainer = null;
        [SerializeField] private ToastItem _Prefab = null;
        [SerializeField] private Transform _PoolContainer = null;
        [SerializeField] private ToastsLibrary _Library = null;

        public override void InstallBindings()
        {
            Container.Bind<Transform>().FromInstance(_PoolContainer).WhenInjectedInto<ToastsPool>();
            Container.BindMemoryPool<ToastItem, ToastsPool>().WithInitialSize(0)
                .FromComponentInNewPrefab(_Prefab).UnderTransform(_PoolContainer);
            Container.Bind<ToastsProducer>().AsSingle();

            Container.Bind<ToastsLibrary>().FromInstance(_Library).AsSingle();
            Container.Bind<Canvas>().FromInstance(_Canvas).WhenInjectedInto<ToastsManager>();
            Container.Bind<Transform>().FromInstance(_ItemsContainer).AsSingle().WhenInjectedInto<ToastsManager>();
        }
    }
}
