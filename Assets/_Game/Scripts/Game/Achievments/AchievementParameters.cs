﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.Achievments
{
    [CreateAssetMenu(fileName = nameof(AchievementParameters), menuName = DefinitionsHelper.Parameters + nameof(AchievementParameters))]
    public class AchievementParameters : ScriptableObject
    {
        public IdValue[] ScoreForGameStages = new IdValue[0];
        public IdValue[] GemsForGameStages = new IdValue[0];
        public IdValue[] TotalScoreStages = new IdValue[0];
        public IdValue[] GamesPlayedStages = new IdValue[0];
        public IdValue[] ContinuousSessionStages = new IdValue[0];
        public IdValue[] PurchaseStages = new IdValue[0];
        public IdValue[] SkinsBoughtStages = new IdValue[0];
        public IdValue[] RewardedViewStages = new IdValue[0];
        public IdValue[] ShareResultStages = new IdValue[0];

        public IdValue TurnsOnPlatform = new IdValue();
        public string AdsDisableId = "";
        public string NoGemsForGameId = "";
        public string NoTurnsForGameId = "";
        public string ViewGDPRId = "";
        public string LeaderboardLeaderId = "";
        public string DisableSettingsInPauseId = "";
        public string ShareGameId = "";
        public string ShareResultId = "";
        public string ViewAllSocialId = "";
        public string ChangeLanguageId = "";
        public string DisableNotifications = "";
    }

    [Serializable]
    public struct IdValue
    {
        [SerializeField] private string _Name;
        [SerializeField] private string _Id;
        [SerializeField] private int _Value;

        public string name => _Name;
        public string id => _Id;
        public int value => _Value;

        public IdValue(string name, string id, int value)
        {
            _Name = name;
            _Id = id;
            _Value = value;
        }
    }
}