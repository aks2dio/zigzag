﻿using _Game.Scripts.Game.Statistics;
using _Game.Scripts.GooglePlay;
using Core;

namespace _Game.Scripts.Game.Achievments
{
    public class AchievementsManager : LoggedClass
    {
        private readonly GooglePlayServiceManager _gpManager;
        private readonly AchievementParameters _parameters;

        ////////////////////////////////////////////////////////////

        public AchievementsManager(AchievementParameters parameters, GooglePlayServiceManager gpManager)
        {
            _gpManager = gpManager;
            _parameters = parameters;

            if (_gpManager == null) Error($"{nameof(_gpManager)} is null!");
            if (_parameters == null) Error($"{nameof(_parameters)} is null!");
        }

        ////////////////////////////////////////////////////////////

        public void ReportScore(SaveStatisticsData data)
        {
            ReportData(_parameters.TotalScoreStages, data.totalScore);
            ReportData(_parameters.ScoreForGameStages, data.maxScoreInGame);
        }

        public void ReportGems(SaveStatisticsData data)
        {
            ReportData(_parameters.GemsForGameStages, data.maxGemsInGame);
            ReportData(_parameters.NoGemsForGameId, data.minGemsInGame == 0);
        }

        public void ReportTurns(SaveStatisticsData data)
        {
            ReportData(_parameters.NoTurnsForGameId, data.minTurnsInGame == 0);
            ReportData(_parameters.TurnsOnPlatform.id,
                data.maxTurnsOnPlatform >= _parameters.TurnsOnPlatform.value);
        }

        public void ReportSessions(SaveStatisticsData data) =>
            ReportData(_parameters.ContinuousSessionStages, data.maxContinuousDaysPlayed);

        public void ReportGamesPlayed(SaveStatisticsData data) =>
            ReportData(_parameters.GamesPlayedStages, data.gamesPlayed);

        public void ReportLeaderboard(SaveStatisticsData data) =>
            ReportData(_parameters.LeaderboardLeaderId, data.minLeaderboardPosition == 1);

        public void ReportPurchases(SaveStatisticsData data) =>
            ReportData(_parameters.PurchaseStages, data.purchases);

        public void ReportSkinsBought(SaveStatisticsData data) =>
            ReportData(_parameters.SkinsBoughtStages, data.skinsBought);

        public void ReportRewardedViews(SaveStatisticsData data) =>
            ReportData(_parameters.RewardedViewStages, data.rewardedViewed);

        public void ReportAdsDisabled(SaveStatisticsData data) =>
            ReportData(_parameters.AdsDisableId, data.disabledAds);

        public void ReportDisableSettings(SaveStatisticsData data)
        {
            ReportData(_parameters.DisableSettingsInPauseId, data.disableMusicInPause && data.disableSoundInPause && data.disabledVibrationInPause);
            ReportData(_parameters.DisableNotifications, data.disabledNotifications);
        }

        public void ReportShares(SaveStatisticsData data)
        {
            ReportData(_parameters.ShareResultStages, data.shareGame + data.shareResult);
            ReportData(_parameters.ShareResultId, data.shareResult > 0);
            ReportData(_parameters.ShareGameId, data.shareGame > 0);
        }

        public void ReportSocialViews(SaveStatisticsData data) =>
            ReportData(_parameters.ViewAllSocialId,
                data.viewVk && data.viewFacebook && data.viewInstagram && data.viewLinkedin);

        public void ReportChangeLanguage(SaveStatisticsData data) =>
            ReportData(_parameters.ChangeLanguageId, data.changeLanguage);

        public void ReportGDPR(SaveStatisticsData data) =>
            ReportData(_parameters.ViewGDPRId, data.readPartnersData && data.readPrivacyPolicy && data.readTermsofUse);

        public void FullReport(SaveStatisticsData data)
        {
            ReportScore(data);
            ReportGems(data);
            ReportTurns(data);
            ReportSessions(data);
            ReportGamesPlayed(data);
            ReportLeaderboard(data);
            ReportPurchases(data);
            ReportSkinsBought(data);
            ReportRewardedViews(data);
            ReportAdsDisabled(data);
            ReportDisableSettings(data);
            ReportChangeLanguage(data);
            ReportSocialViews(data);
            ReportShares(data);
            ReportGDPR(data);
        }

        ////////////////////////////////////////////////////////////

        private void ReportData(IdValue[] stages, int value)
        {
            foreach (var stage in stages)
                ReportData(stage, value);
        }

        private void ReportData(IdValue stage, int value) =>
            ReportData(stage.id, (double) value / stage.value * 100f);

        private void ReportData(string id, double progress)
        {
            _gpManager.achievements.ReportAchievementProgress(id, progress);
            Log($"Report {id}: {progress}");
        }

        private void ReportData(string id, bool value)
        {
            if (value == false)
                return;

            _gpManager.achievements.CompleteAchievement(id);
            Log($"Report {id} complete");
        }
    }
}