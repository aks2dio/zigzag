﻿using System;
using System.Threading;
using System.Threading.Tasks;
using _Game.Scripts.Game.IAP.Shop.Library;
using _Game.Scripts.AssetsLoading;
using _Game.Scripts.Game.Localization.Components;
using Core.Components;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Game.IAP.Shop.View.Item
{
    class IAPShopItem : MyMonoBehaviour, I_IAPShopItem
    {
        [SerializeField] private Image _Image = null;
        [SerializeField] private LocalizedUniversalText _Header = null;
        [SerializeField] private UniversalText _Price = null;
        [SerializeField] private GameObject _BestOffer = null;
        [SerializeField] private Button _Button = null;

        public I_IAPShopLibraryItem data { get; private set; }

        public event Action<I_IAPShopItem> onClick;

        private CancellationTokenSource tokenSource;

        private AssetLoaderToImage _loaderCache;
        private AssetLoaderToImage _imageLoader => _loaderCache ??= new AssetLoaderToImage(_Image);

        //////////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(_Image);
            CheckField(_Header);
            CheckField(_Price);
            CheckField(_Button);
            CheckField(_BestOffer);
        }

        private void OnDisable()
        {
            StopWaiting();
            _imageLoader.Unload();
        }

        //////////////////////////////////////////////////

        public void Construct(I_IAPManager iapManager, I_IAPShopLibraryItem data)
        {
            if (Equals(this.data, data))
                return;

            this.data = data;
            SetData(iapManager, data);
        }

        private void SetData(I_IAPManager iapManager, I_IAPShopLibraryItem data)
        {
            _BestOffer.SetActive(data.isBestOffer);
            _Price.SetText(data.price);
            _Header.SetKey(data.header);

            _imageLoader.Load(data.imageReference);

            Waiting(ref tokenSource,
                () => iapManager.isInitialized,
                () => _Price.SetText(GetLocalizedPrice(iapManager, data)));

            _Button.onClick.AddListener(Click);
        }

        private string GetLocalizedPrice(I_IAPManager iapManager, I_IAPShopLibraryItem data)
        {
            if (!iapManager.isInitialized)
                return data.price;

            if (iapManager.consumerProducts.TryGetPurchase(data.name, out var item) == false)
                if (iapManager.nonConsumerProducts.TryGetPurchase(data.name, out item) == false)
                    return data.price;

            foreach (var p in iapManager.storeProducts)
                if (p.definition.id == item.CurrentId)
                    return p.metadata.localizedPriceString;

            return data.price;
        }

        //////////////////////////////////////////////////

        private void Click()
        {
            onClick?.Invoke(this);
            UnityEngine.Debug.Log("IAPShopItem clicked");
        }

        //////////////////////////////////////////////////

        private void Waiting(ref CancellationTokenSource tokenSource, Func<bool> waitUntil, Action callback)
        {
            StopWaiting();
            tokenSource = new CancellationTokenSource();
            WaitingRoutine(tokenSource.Token, waitUntil, callback);
        }

        private async void WaitingRoutine(CancellationToken token, Func<bool> waitUntil, Action callback)
        {
            if (waitUntil == null || callback == null)
                return;

            while (waitUntil() == false && !token.IsCancellationRequested)
            {
                await Task.Delay(100);
                await Task.Yield();
            }

            if (!token.IsCancellationRequested)
                callback();
        }

        private void StopWaiting() =>
            tokenSource?.Cancel();
    }
}
