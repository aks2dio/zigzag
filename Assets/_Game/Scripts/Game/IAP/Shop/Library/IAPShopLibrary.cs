﻿using System.Linq;
using UnityEngine;

namespace _Game.Scripts.Game.IAP.Shop.Library
{
    [CreateAssetMenu(fileName = nameof(IAPShopLibrary), menuName = DefinitionsHelper.Parameters + nameof(IAPShopLibrary))]
    class IAPShopLibrary : ScriptableObject, I_IAPShopLibrary
    {
        [SerializeField] private IAPShopLibraryItem[] _Items = new IAPShopLibraryItem[0];

        public I_IAPShopLibraryItem[] items => _Items != null ?
            _Items.Select(i => (I_IAPShopLibraryItem) i).ToArray() :
            new I_IAPShopLibraryItem[0];

        ///////////////////////////////////////////////////////////////////////

        public bool TryGetItem(string name, out I_IAPShopLibraryItem item)
        {
            item = default;

            if (Contains(name) == false)
                return false;

            item = GetItem(name);
            return true;
        }

        public I_IAPShopLibraryItem GetItem(string name)
        {
            foreach (var i in _Items)
                if (i.name == name)
                    return i;

            return null;
        }

        public bool Contains(string name)
        {
            foreach (var i in _Items)
                if (i.name == name)
                    return true;

            return false;
        }
    }
}
