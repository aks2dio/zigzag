﻿using System.Collections.Generic;
using _Game.Scripts.Game.IAP.Library;
using UnityEngine.Purchasing;

namespace _Game.Scripts.Game.IAP
{
    public delegate void OnSuccessConsumable(PurchaseEventArgs args);
    public delegate void OnSuccessNonConsumable(PurchaseEventArgs args);
    public delegate void OnFailedPurchase(Product product, PurchaseFailureReason failureReason);

    interface I_IAPManager : IStoreListener
    {
        event OnSuccessConsumable onPurchaseConsumable;
        event OnSuccessNonConsumable onPurchaseNonConsumable;
        event OnFailedPurchase onPurchaseFailed;

        I_IAPLibrary nonConsumerProducts { get; }
        I_IAPLibrary consumerProducts { get; }
        IEnumerable<Product> storeProducts { get; }

        bool isInitialized { get; }
        void InitializePurchasing();

        bool CheckBuyState(string id);
        void BuyConsumable(string id);
        void BuyNonConsumable(string id);

        void RestorePurchases();
    }
}
