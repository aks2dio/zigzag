﻿
namespace _Game.Scripts.Game.IAP.Library
{
    public interface I_IAPLibrary
    {
        I_IAPProduct[] Products { get; }

        bool ContainsPurchaseItem(string name);
        bool ContainsPurchaseItemId(string id);

        I_IAPProduct GetPurchaseItem(string name);
        I_IAPProduct GetPurchaseItemById(string id);

        bool TryGetPurchase(string name, out I_IAPProduct item);
        bool TryGetPurchaseItemById(string id, out I_IAPProduct item);
    }
}
