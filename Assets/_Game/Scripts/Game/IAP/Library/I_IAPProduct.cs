﻿using UnityEngine.Purchasing;

namespace _Game.Scripts.Game.IAP.Library
{
    public interface I_IAPProduct
    {
        string Name { get; }
        IDs IDs { get; }
        string CurrentId { get; }

        bool ContainsId(string id);
    }
}
