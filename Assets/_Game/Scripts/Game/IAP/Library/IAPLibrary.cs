﻿using System.Linq;
using UnityEngine;

namespace _Game.Scripts.Game.IAP.Library
{
    [CreateAssetMenu(fileName = nameof(IAPLibrary), menuName = DefinitionsHelper.Parameters + nameof(IAPLibrary))]
    public class IAPLibrary : ScriptableObject, I_IAPLibrary
    {
        [SerializeField] private bool _UniqueId = true;
        [SerializeField] private IAPProduct[] _Products = new IAPProduct[0];

        public I_IAPProduct[] Products => _Products != null ? 
            _Products.Select(p => (I_IAPProduct)p).ToArray() : 
            new I_IAPProduct[0];

        ///////////////////////////////////////////////

        #region MONO_BEHAVIOUR

        private void OnValidate()
        {
            CorrectRepeatingElements();
        }

        private void CorrectRepeatingElements()
        {
            if (_UniqueId == false)
                return;

            foreach (var i in Enumerable.Range(0, Products.Length).Reverse())
                if (Products.Count(p => p.Name == Products[i].Name) > 1)
                    _Products[i] = new IAPProduct();
        }

        #endregion // MONO_BEHAVIOUR

        #region PUBLIC_METHODS

        public bool ContainsPurchaseItem(string name) 
            => Products.Any(p => p.Name == name);

        public bool ContainsPurchaseItemId(string id)
            => Products.Any(p => p.ContainsId(id));


        public I_IAPProduct GetPurchaseItem(string name)
            => Products.FirstOrDefault(p => p.Name == name);

        public I_IAPProduct GetPurchaseItemById(string id)
            => Products.FirstOrDefault(p => p.ContainsId(id));


        public bool TryGetPurchase(string name, out I_IAPProduct item)
        {
            item = null;

            if (ContainsPurchaseItem(name) == false)
                return false;

            item = GetPurchaseItem(name);
            return true;
        }

        public bool TryGetPurchaseItemById(string id, out I_IAPProduct item)
        {
            item = null;

            if (ContainsPurchaseItemId(id) == false)
                return false;

            item = GetPurchaseItemById(id);
            return true;
        }

        #endregion // PUBLIC_METHODS
    }
}