﻿using System;
using _Game.Scripts.Game.IAP.Library;
using _Game.Scripts.Game.IAP.Shop.Library;
using Core;

namespace _Game.Scripts.Game.IAP.Management
{
    class PurchasesManager : LoggedClass, IPurchasesManager
    {
        private readonly I_IAPShopLibrary _iapShop;
        private readonly ItemTypedPurchasesManager[] _managers;

        public event Action<I_IAPProduct, I_IAPShopLibraryItem> onSuccess {
            add => Array.ForEach(_managers, m => m.onSuccess += value);
            remove => Array.ForEach(_managers, m => m.onSuccess -= value);
        }
        public event Action<I_IAPProduct, I_IAPShopLibraryItem> onFailed {
            add => Array.ForEach(_managers, m => m.onFailed += value);
            remove => Array.ForEach(_managers, m => m.onFailed -= value);
        }

        ////////////////////////////////////////////////////

        public PurchasesManager(I_IAPShopLibrary iapShop, params ItemTypedPurchasesManager[] managers)
        {
            _iapShop = iapShop;
            _managers = managers;
        }

        ////////////////////////////////////////////////////

        public bool Buy(string itemName)
        {
            if (_iapShop.TryGetItem(itemName, out var shopItem) == false) {
                Error($"IAP Shop doesn't contain '{itemName}'");
                return false;
            }

            return Buy(shopItem);
        }

        public bool Buy(I_IAPShopLibraryItem shopItem)
        {
            foreach (var m in _managers)
                if (m.Buy(shopItem))
                {
                    Log($"Buy {shopItem.name}");
                    return true;
                }

            Error($"There aren't managers, who can buy '{shopItem.name}'!");
            return false;
        }

        public bool IsAvailable(string itemName) => 
            _iapShop.TryGetItem(itemName, out var shopItem) 
            && IsAvailable(shopItem);

        public bool IsAvailable(I_IAPShopLibraryItem shopItem)
        {
            foreach (var m in _managers)
                if (m.IsAvailable(shopItem))
                    return true;

            return false;
        }
    }
}
