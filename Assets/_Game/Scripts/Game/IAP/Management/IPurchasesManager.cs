﻿using System;
using _Game.Scripts.Game.IAP.Library;
using _Game.Scripts.Game.IAP.Shop.Library;

namespace _Game.Scripts.Game.IAP.Management
{
    interface IPurchasesManager
    {
        event Action<I_IAPProduct, I_IAPShopLibraryItem> onSuccess;
        event Action<I_IAPProduct, I_IAPShopLibraryItem> onFailed;

        bool Buy(string itemName);
        bool Buy(I_IAPShopLibraryItem shopItem);

        bool IsAvailable(string itemName);
        bool IsAvailable(I_IAPShopLibraryItem shopItem);
    }
}
