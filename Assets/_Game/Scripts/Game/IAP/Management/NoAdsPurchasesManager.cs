﻿using System;
using _Game.Scripts.Game.IAP.Library;
using _Game.Scripts.Game.IAP.Management.Saving;
using _Game.Scripts.Game.IAP.Shop.Library;
using _Game.Scripts.Game.Statistics;

namespace _Game.Scripts.Game.IAP.Management
{
    class NoAdsPurchasesManager : ItemTypedPurchasesManager
    {
        private readonly PurchasingData _purchasingData;

        protected override Action<string> _buyMethods => _iapManager.BuyNonConsumable;
        protected override I_IAPLibrary _library => _iapManager.nonConsumerProducts;

        ///////////////////////////////////////////////////////

        public NoAdsPurchasesManager(I_IAPManager iapManager, I_IAPShopLibrary iapShop, 
            PurchasingData purchasingData, StatisticsData statisticsData) : base(iapManager, iapShop, statisticsData)
        {
            _purchasingData = purchasingData;
        }

        ///////////////////////////////////////////////////////

        public override bool IsAvailable(I_IAPShopLibraryItem shopItem)
            => shopItem.type == IAPProductType.NoAds && Contains(shopItem) && !_purchasingData.noAds;

        ///////////////////////////////////////////////////////

        protected override void OnSuccessPurchasingActions(I_IAPProduct item, I_IAPShopLibraryItem shopItem)
        {
            if (shopItem.type != IAPProductType.NoAds)
                return;

            _statistics.SetAdsDisabled();
            _purchasingData.SetNoAds(true);
        }

        protected override void OnFailedPurchasingActions(I_IAPProduct item, I_IAPShopLibraryItem shopItem)
        {

        }
    }
}
