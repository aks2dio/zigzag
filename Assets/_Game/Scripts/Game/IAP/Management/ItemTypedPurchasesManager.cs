﻿using System;
using _Game.Scripts.Game.IAP.Library;
using _Game.Scripts.Game.IAP.Shop.Library;
using _Game.Scripts.Game.Statistics;
using UnityEngine.Purchasing;

namespace _Game.Scripts.Game.IAP.Management
{
    abstract class ItemTypedPurchasesManager
    {
        protected I_IAPManager _iapManager;
        protected I_IAPShopLibrary _iapShop;
        protected StatisticsData _statistics;

        protected abstract Action<string> _buyMethods { get; }
        protected abstract I_IAPLibrary _library { get; }

        public event Action<I_IAPProduct, I_IAPShopLibraryItem> onSuccess;
        public event Action<I_IAPProduct, I_IAPShopLibraryItem> onFailed;

        ///////////////////////////////////////////////////////

        public ItemTypedPurchasesManager(I_IAPManager iapManager, I_IAPShopLibrary iapShop, StatisticsData statisticsData)
        {
            _iapShop = iapShop;
            _iapManager = iapManager;
            _statistics = statisticsData;

            _iapManager.onPurchaseNonConsumable += OnSuccessPurcahsing;
            _iapManager.onPurchaseConsumable += OnSuccessPurcahsing;
            _iapManager.onPurchaseFailed += OnFailedPurchasing;
        }

        ///////////////////////////////////////////////////////

        public bool Buy(I_IAPShopLibraryItem shopItem)
        {
            if (IsAvailable(shopItem) == false)
                return false;

            _buyMethods?.Invoke(shopItem.name);
            return true;
        }

        public abstract bool IsAvailable(I_IAPShopLibraryItem shopItem);

        ///////////////////////////////////////////////////////

        protected virtual void OnSuccessPurchasingActions(I_IAPProduct item, I_IAPShopLibraryItem shopItem)
        { }

        protected virtual void OnFailedPurchasingActions(I_IAPProduct item, I_IAPShopLibraryItem shopItem)
        { }

        protected bool Contains(I_IAPShopLibraryItem shopItem)
            => _library.ContainsPurchaseItem(shopItem.name);

        ///////////////////////////////////////////////////////

        private void OnSuccessPurcahsing(PurchaseEventArgs args)
        {
            var id = args.purchasedProduct.definition.id;

            if (_library.TryGetPurchaseItemById(id, out var item) == false)
                return;

            if (_iapShop.TryGetItem(item.Name, out var shopItem) == false)
                return;

            _statistics.AddPurchase();
            OnSuccessPurchasingActions(item, shopItem);
            onSuccess?.Invoke(item, shopItem);
        }

        private void OnFailedPurchasing(Product product, PurchaseFailureReason failureReason)
        {
            var id = product.definition.id;

            if (_library.TryGetPurchaseItemById(id, out var item) == false)
                return;

            if (_iapShop.TryGetItem(item.Name, out var shopItem) == false)
                return;

            OnFailedPurchasingActions(item, shopItem);
            onFailed?.Invoke(item, shopItem);
        }
    }
}
