﻿using System;
using System.Collections;
using UnityEngine;

namespace _Game.Scripts.Game.IAP.View
{
    public class IAPCanvas : MonoBehaviour
    {
        #region SERIALIZE_FIELDS

        [Header("Attachments")]
        [SerializeField] private Animator _Animator = null;
        [SerializeField] private CanvasGroup _CanvasGroup = null;

        [Header("Animator configs")]
        [SerializeField] private string _ErrorTrigger = "Error";

        #endregion // SERIALIZE_FIELDS

        #region PRIVATE_FIELDS
        private bool _showError { get; set; }
        private bool _active => gameObject.activeInHierarchy;
        private bool _canBeEnabled => true;
        private bool _canBeDisabled => _active && _disabling == null;

        private Coroutine _fading { get; set; } = null;
        private Coroutine _disabling { get; set; } = null;
        private Coroutine _errorShowing { get; set; } = null;
        #endregion // PRIVATE_FIELDS

        ////////////////////////////////////////////////////////

        #region MONO_BEHAVIOUR

        private void OnDisable()
        {
            StopDisable();
            StopError();
        }

        #endregion // MONO_BEHAVIOUR

        #region ENABLE

        [ContextMenu("Enable")]
        public void Enable()
        {
            if (_canBeEnabled == false)
                return;

            StopDisable();

            gameObject.SetActive(true);
            SetUnvisible();
            Unfade();

            UnityEngine.Debug.Log("IAP Canvas Enabled");
        }

        #endregion // ENABLE

        #region ERROR

        [ContextMenu("Error")]
        public void ForceError()
        {
            if (_active == false || _showError)
                return;

            if (_Animator == null) {
                ErrorCallback();
                return;
            }

            _showError = true;
            _Animator.SetTrigger(_ErrorTrigger);
            _errorShowing = StartCoroutine(ShowingError(ErrorCallback));

            UnityEngine.Debug.Log("IAP Canvas Show error");
        }

        private void StopError()
        {
            if (_errorShowing != null) {
                StopCoroutine(_errorShowing);
                _errorShowing = null;
            }
            _showError = false;
        }

        private void ErrorCallback()
        {
            _showError = false;
            _errorShowing = null;
            Disable();
        }

        private IEnumerator ShowingError(Action callback = null)
        {
            yield return new WaitUntil(() => UnityEngine.Input.GetMouseButtonUp(0));
            callback?.Invoke();
        }

        #endregion // ERROR

        #region DISABLE

        [ContextMenu("Disable")]
        public void Disable()
        {
            if (_canBeDisabled == false)
                return;

            if (_Animator == null) {
                DisableCallback();
                return;
            }

            Fade(DisableCallback);
            UnityEngine.Debug.Log("IAP Canvas Disabled");
        }

        private void DisableCallback()
        {
            _disabling = null;
            gameObject.SetActive(false);
        }

        private void StopDisable()
        {
            if (_disabling != null) {
                StopCoroutine(_disabling);
                _disabling = null;
            }
        }

        #endregion // DISABLE

        #region FADE\UNFADE

        private void SetUnvisible()
        {
            if (_CanvasGroup)
                _CanvasGroup.alpha = 0;
        }

        private void Fade(Action callback = null)
        {
            StopFading();
            _fading = StartCoroutine(ChangeAlpha(0, 0.5f, callback));
        }

        private void Unfade(Action callback = null)
        {
            StopFading();
            _fading = StartCoroutine(ChangeAlpha(1, 0.5f, callback));
        }

        private void StopFading()
        {
            if (_fading != null) {
                StopCoroutine(_fading);
                _fading = null;
            }
        }

        private IEnumerator ChangeAlpha(float targetAlpha, float time, Action callback = null)
        {
            var t = 0f;
            var startAlpha = _CanvasGroup?.alpha ?? 0;

            while (_CanvasGroup && t < time) {
                _CanvasGroup.alpha = Mathf.Lerp(startAlpha, targetAlpha, t / time);
                t += Time.deltaTime;
                yield return null;
            }

            if (_CanvasGroup)
                _CanvasGroup.alpha = targetAlpha;

            callback?.Invoke();
        }

        #endregion // FADE\UNFADE
    }
}
