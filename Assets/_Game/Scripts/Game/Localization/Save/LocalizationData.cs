﻿using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Save;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Save
{
    public class LocalizationData : DataObject<SaveLocalizationData>
    {
        public bool selected { get; private set; }
        public SystemLanguage language { get; private set; }

        private readonly IAnalyticsManager _analytics;


        protected override string _save_key { get; } = "localization_data";

        /////////////////////////////////////////////////////

        public LocalizationData(SaveManager saveManager, IAnalyticsManager analytics) : base(saveManager)
        {
            _analytics = analytics;
        }

        /////////////////////////////////////////////////////

        public void SetLanguage(SystemLanguage language)
        {
            if (this.language == language && selected)
                return;

            selected = true;
            this.language = language;

            _analytics.SetLanguage(AnalyticsHelper.GetSetLanguageEventData(language));

            InvokeOnChanged();
            Save();
        }

        /////////////////////////////////////////////////////

        public override SaveLocalizationData CreateSaveData() =>
            new SaveLocalizationData()
            {
                selected = selected,
                language = language
            };

        protected override void OnApply(SaveLocalizationData saveData, SaveApplyType applyType)
        {
            selected = ResultDataToApply(saveData.selected, selected, applyType);
            language = ResultDataToApply(saveData.language, language, applyType);
        }
    }
}
