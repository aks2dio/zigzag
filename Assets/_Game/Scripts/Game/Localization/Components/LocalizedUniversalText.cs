﻿using Core.Components;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Components
{
    [RequireComponent(typeof(UniversalText))]
    public class LocalizedUniversalText : LocalizedText<UniversalText>
    {
        protected override void SetText(string text) =>
            _textComponent.SetText(text);
    }
}
