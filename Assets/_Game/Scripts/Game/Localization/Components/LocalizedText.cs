﻿using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Localization.Components
{
    public abstract class LocalizedText<T> : LocalizedComponent where T : MonoBehaviour
    {
        [SerializeField] private string _Key = "";
        [SerializeField] private bool _Uppercase = false;

        private T _textComponentCache;
        protected T _textComponent => _textComponentCache
            ? _textComponentCache
            : _textComponentCache = GetComponent<T>();

        ////////////////////////////////////////////////////

        protected override async void SetContent(SystemLanguage language)
        {
            if (string.IsNullOrEmpty(_Key))
                return;

            var text = await _Manager.LocalizeText(language, _Key);
            if (_Uppercase)
                text = text.ToUpper();

            Log($"Set lang: {language}");
            SetText(text);
        }

        ////////////////////////////////////////////////////

        protected abstract void SetText(string text);

        ////////////////////////////////////////////////////

        public void SetKey(string key)
        {
            if (key == _Key)
                return;

            _Key = key;
            Log($"Set key: {key}");

            UpdateValue();
        }
    }
}
