﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Parameters
{
    public abstract class LocalizationAsset<T> : ScriptableObject
    {
        public abstract IEnumerable<LocalizationEntity<T>> entities { get; }

        ////////////////////////////////////////////////////////

        public Dictionary<string, T> CreateDictionary() =>
            entities.ToDictionary(e => e.key, e => e.value);

    }

    [Serializable]
    public class LocalizationEntity<T>
    {
        [SerializeField] private string _Key = "";
        [SerializeField] private T _Value = default;

        public string key => _Key;
        public T value => _Value;

        ////////////////////////////////////////////////////////

        protected LocalizationEntity(string key, T value)
        {
            _Key = key;
            _Value = value;
        }
    }
}
