﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Parameters
{
    public class LocalizationSpriteAsset : LocalizationAsset<Sprite>
    {
        [SerializeField] private LocalizationEntitySprite[] _Entities = new LocalizationEntitySprite[0];

        public override IEnumerable<LocalizationEntity<Sprite>> entities => _Entities;
    }

    [Serializable]
    public class LocalizationEntitySprite : LocalizationEntity<Sprite>
    {
        protected LocalizationEntitySprite(string key, Sprite value) : base(key, value)
        { }
    }
}
