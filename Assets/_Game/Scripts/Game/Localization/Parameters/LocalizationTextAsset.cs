﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.Game.Localization.Parameters
{
    [CreateAssetMenu(fileName = nameof(LocalizationTextAsset), menuName = DefinitionsHelper.Parameters + nameof(LocalizationTextAsset))]
    public class LocalizationTextAsset : LocalizationAsset<string>
    {
        [SerializeField] private LocalizationEntityString[] _Entities = new LocalizationEntityString[0];

        public override IEnumerable<LocalizationEntity<string>> entities => _Entities;
    }

    [Serializable]
    public class LocalizationEntityString : LocalizationEntity<string>
    {
        protected LocalizationEntityString(string key, string value) : base(key, value)
        { }
    }
}
