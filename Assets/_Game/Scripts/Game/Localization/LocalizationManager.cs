﻿using System.Collections.Generic;
using System.Threading.Tasks;
using _Game.Scripts.AssetsLoading;
using _Game.Scripts.Game.Localization.Parameters;
using Core.Extensions;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace _Game.Scripts.Game.Localization
{
        public class LoadedLocalizationData
    {
        public readonly SystemLanguage language;
        public readonly Sprite icon;
        public readonly TMP_FontAsset font;
        public readonly Dictionary<string, string> library;

        ////////////////////////////////////////////////////////

        public LoadedLocalizationData(SystemLanguage language, Sprite icon, TMP_FontAsset font, Dictionary<string, string> library)
        {
            this.language = language;
            this.icon = icon;
            this.font = font;
            this.library = library;
        }
    }

    public class LocalizationManager : MyMonoBehaviour
    {
        [Inject] private Save.LocalizationData _Data { get; }
        [Inject] private LocalizationParameters _Parameters { get; }


        [SerializeField] private UnityEvent _OnChangeLanguage = new UnityEvent();

        private LoadedLocalizationData _currentLocalizationData;

        private readonly AssetLoaderByReference<Sprite> _iconLoader = new AssetLoaderByReference<Sprite>();
        private readonly AssetLoaderByReference<TMP_FontAsset> _fontLoader = new AssetLoaderByReference<TMP_FontAsset>();
        private readonly AssetLoaderByReference<LocalizationTextAsset> _libraryLoader = new AssetLoaderByReference<LocalizationTextAsset>();


        public UnityEvent onChangeLanguage => _OnChangeLanguage;
        public IEnumerable<LocalizationData> localizations => _Parameters.localizations;
        public SystemLanguage currentLanguage => _currentLocalizationData?.language ?? _Parameters.defaultLanguage;

        ////////////////////////////////////////////////////////

        private void Awake() =>
            SetLanguage(DetectInitialLanguage());

        private SystemLanguage DetectInitialLanguage()
        {
            if (_Data.selected)
                return _Data.language;

            var systemLanguage = Application.systemLanguage;
            if (_Parameters.autoDetectInitialLanguage && _Parameters.IsAvailable(systemLanguage))
                return systemLanguage;

            return _Parameters.defaultLanguage;
        }

        ////////////////////////////////////////////////////////

        public async void SetLanguage(SystemLanguage language)
        {
            if (TryGetLocalizationData(language, out var data) == false)
                return;

            var icon = _iconLoader.Load(data.iconReference);
            var font = _fontLoader.Load(data.fontReference);
            var library = _libraryLoader.Load(data.textAssetReference);

            await Task.WhenAll(icon, font, library);

            _currentLocalizationData = new LoadedLocalizationData(language,
                icon.Result, font.Result, library.Result.CreateDictionary());

            Log($"Set language: {language}");
            _Data.SetLanguage(language);
            _OnChangeLanguage?.Invoke();
        }

        public string LocalizeText(string key)
        {
            if (_currentLocalizationData == null)
                return "";

            if (_currentLocalizationData.library.TryGetValue(key, out var text) == false)
            {
                Error($"Can't localize {key}");
                return "";
            }

            return text;
        }

        public async Task<string> LocalizeText(SystemLanguage language, string key)
        {
            if (_currentLocalizationData?.language == language)
                return LocalizeText(key);

            if (TryGetLocalizationData(language, out var data) == false)
            {
                Error($"Can't localize for {language}");
                return "";
            }

            var library = await _libraryLoader.Load(data.textAssetReference);
            foreach (var entity in library.entities)
                if (entity.key == key)
                    return entity.value;

            Error($"Can't localize {key} for {language}");
            return "";
        }

        public TMP_FontAsset GetFont() => _currentLocalizationData.font;
        public Task<TMP_FontAsset> GetFont(SystemLanguage language)
        {
            if (_currentLocalizationData?.language == language)
                return Task.FromResult(_currentLocalizationData.font);

            if (TryGetLocalizationData(language, out var data) == false)
            {
                Error($"Can't find font for {language}");
                return null;
            }

            return _fontLoader.Load(data.fontReference);
        }

        public Sprite GetIcon() => _currentLocalizationData.icon;
        public Task<Sprite> GetIcon(SystemLanguage language)
        {
            if (_currentLocalizationData?.language == language)
                return Task.FromResult(_currentLocalizationData.icon);

            if (TryGetLocalizationData(language, out var data) == false)
            {
                Error($"Can't find icon for {language}");
                return null;
            }

            return _iconLoader.Load(data.iconReference);
        }

        ////////////////////////////////////////////////////////

        private bool TryGetLocalizationData(SystemLanguage language, out LocalizationData data)
        {
            foreach (var loc in localizations)
                if (loc.language == language)
                {
                    data = loc;
                    return true;
                }

            data = null;
            return false;
        }
    }
}
