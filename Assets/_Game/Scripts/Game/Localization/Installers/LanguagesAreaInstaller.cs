﻿using _Game.Scripts.Game.Localization.View;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Localization.Installers
{
    public class LanguagesAreaInstaller : MonoInstaller<LanguagesAreaInstaller>
    {
        [SerializeField] private Transform _Container;
        [SerializeField] private GameObject _Prefab;

        public override void InstallBindings()
        {
            Container.Bind<Transform>().FromInstance(_Container).WhenInjectedInto<LanguagesArea>();
            Container.BindFactory<LanguageButton, LanguageButtonFactory>().FromComponentInNewPrefab(_Prefab).AsSingle();
        }
    }
}
