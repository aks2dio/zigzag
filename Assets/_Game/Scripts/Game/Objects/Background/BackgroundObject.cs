﻿using _Game.Scripts.Instruments.Following;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Game.Objects.Background
{
    [RequireComponent(typeof(PositionFollower))]
    class BackgroundObject : MyMonoBehaviour
    {
        [SerializeField] private Image _ImageComponent = null;
        [SerializeField] private AspectRatioFitter _Fitter = null;

        ////////////////////////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(_ImageComponent);
            CheckField(_Fitter);
        }

        ////////////////////////////////////////////////////////////////

        public void SetImage(Sprite sprite) =>
            SetImage(sprite, Color.white);

        public void SetImage(Sprite sprite, Color color)
        {
            _ImageComponent.sprite = sprite;
            _ImageComponent.color = color;
            UpdateAspectRatio(sprite);
        }

        ////////////////////////////////////////////////////////////////

        private void UpdateAspectRatio(Sprite sprite)
        {
            if (sprite == null)
                return;

            var rect = sprite.rect;

            _Fitter.aspectRatio = rect.height > 0 ? 
                rect.width / rect.height : 0;
        }
    }
}
