﻿
namespace _Game.Scripts.Game.Objects.Interfaces
{
    public interface IProducer<T>
    {
        T Produce();
        void HandleUnused(T booster);
        void HandleUnused(T[] boosters);
    }
}
