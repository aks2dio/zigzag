﻿using System;
using Core.Interfaces;
using Core.Systems.Reset;

namespace _Game.Scripts.Game.Objects.Interfaces
{
    public interface IRemoteDestroyable : IGameObjectHost
    {
        Action<IRemoteDestroyable> onBecomeDestroyable { get; set; }

        void SetDestroyed();
    }
}
