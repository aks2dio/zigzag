﻿using UnityEngine;

namespace _Game.Scripts.Game.Objects.Interfaces
{
    public interface IPlacedOnSurface
    {
        void SetPositionOnSurface(Vector3 positionOnSurface);
    }
}
