﻿namespace _Game.Scripts.Game.Objects.Shadow
{
    interface IShadowObject
    {
        void Show();
        void Hide();
        void Show(bool status);
    }
}
