﻿using System.Collections;
using Core.Extensions;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Shadow
{
    class ShadowObject : MyMonoBehaviour, IShadowObject
    {
        [Header("Attachments")]
        [SerializeField] private SpriteRenderer _SpriteRenderer = null;

        [Header("Settings")]
        [SerializeField] private float _AnimationTime = 0.1f;
        [SerializeField] private float _ShowAlpha = 1f;
        [SerializeField] private float _HideAlpha = 0f;

        private Coroutine _changeAlphaRoutine;

        /////////////////////////////////////////////////////

        private void OnDestroy() => StopRoutine(ref _changeAlphaRoutine);

        protected override void OnChecking() => CheckField(_SpriteRenderer);

        /////////////////////////////////////////////////////

        [ContextMenu("Show")]
        public void Show() => Show(true);

        [ContextMenu("Hide")]
        public void Hide() => Show(false);

        public void Show(bool status) =>
            ChangeAlpha(status ? _ShowAlpha : _HideAlpha);

        /////////////////////////////////////////////////////

        private void ChangeAlpha(float targetAlpha)
        {
            StopRoutine(ref _changeAlphaRoutine);
            _changeAlphaRoutine = StartCoroutine(ChangeAlphaRoutine(targetAlpha, _AnimationTime));
        }

        private void StopRoutine(ref Coroutine coroutine)
        {
            if (coroutine != null)
                StopCoroutine(coroutine);

            coroutine = null;
        }

        private IEnumerator ChangeAlphaRoutine(float targetAlpha, float time)
        {
            var t = 0f;
            var startAlpha = _SpriteRenderer.color.a;

            while(t < time)
            {
                SetAlpha(Mathf.Lerp(startAlpha, targetAlpha, t / time));
                t += Time.deltaTime;
                yield return null;
            }

            SetAlpha(targetAlpha);

            void SetAlpha(float alpha)
            {
                var color = _SpriteRenderer.color;
                color.a = alpha;
                _SpriteRenderer.color = color;
            }
        }
    }
}
