﻿using _Game.Scripts.Extensions;
using Core.Systems.Reset;

namespace _Game.Scripts.Game.Objects.Moving.Allower
{
    interface IMoveAllower : IResetable
    {
        bool allowed { get; }
        BoolUnityEvent onStatusChanged { get; }
    }
}
