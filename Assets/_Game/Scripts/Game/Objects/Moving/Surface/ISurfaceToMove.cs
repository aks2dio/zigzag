﻿
namespace _Game.Scripts.Game.Objects.Moving.Surface
{
    interface ISurfaceToMove
    {
        bool allowMove { get; }
    }
}
