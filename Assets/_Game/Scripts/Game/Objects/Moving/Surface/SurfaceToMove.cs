﻿using UnityEngine;

namespace _Game.Scripts.Game.Objects.Moving.Surface
{
    [RequireComponent(typeof(Collider))]
    class SurfaceToMove : MonoBehaviour, ISurfaceToMove
    {
        [SerializeField] private bool _AllowMove = false;

        public bool allowMove => _AllowMove;
    }
}
