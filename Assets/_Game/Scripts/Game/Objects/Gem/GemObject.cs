﻿using System;
using Core.Systems.Reset;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Gem
{
    [RequireComponent(typeof(Animator))]
    class GemObject : DestroyableMonoBehaviour, IGem, IResetable
    {
        private Animator _animator;

        public bool isActive { get; private set; } = true;
        public Action<IGem> onCollected { get; set; }

        private const string COLLECT_TRIGGER = "Collect";
        private const string COLLECTING_STATE = "Collecting";

        private const string DEACTIVATE_TRIGGER = "Deactivate";
        private const string DEACTIVATE_STATE = "Deactivating";

        ///////////////////////////////////////////

        #region ATTACHING

        protected override void OnAttaching()
            => Attach(ref _animator);

        #endregion // ATTACHING

        #region PUBLIC_METHODS
        public void Collect()
        {
            if (isActive == false)
                return;

            isActive = false;
            LaunchCollectAnimation();
            onCollected?.Invoke(this);
        }

        public void Deactivate()
        {
            if (isActive == false)
                return;

            isActive = false;
            LaunchDeactivateAnimation();
        }

        public void SetPositionOnSurface(Vector3 positionOnSurface)
        {
            transform.localPosition = positionOnSurface;
        }

        public void OnAnimationStart(AnimatorStateInfo stateInfo)
        { }

        public void OnAnimationEnd(AnimatorStateInfo stateInfo)
        {
            if (stateInfo.IsName(COLLECTING_STATE))
                SetDestroyed();
            if (stateInfo.IsName(DEACTIVATE_STATE))
                SetDestroyed();
        }

        public void Reset()
        {
            isActive = true;
            onCollected = null;

            _animator.ResetTrigger(COLLECT_TRIGGER);
            _animator.ResetTrigger(DEACTIVATE_TRIGGER);
            _animator.WriteDefaultValues();

            transform.localPosition = Vector3.zero;
        }

        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS
        private void LaunchCollectAnimation()
            => LaunchPostDestroyedAnimation(COLLECT_TRIGGER);

        private void LaunchDeactivateAnimation()
            => LaunchPostDestroyedAnimation(DEACTIVATE_TRIGGER);

        private void LaunchPostDestroyedAnimation(string name)
            => _animator.SetTrigger(name);

        #endregion // PRIVATE_METHODS
    }
}
