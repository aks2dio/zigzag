﻿using System;
using _Game.Scripts.Game.Objects.AnimatedObjects;
using _Game.Scripts.Game.Objects.Interfaces;

namespace _Game.Scripts.Game.Objects.Gem
{
    public interface IGem : IAnimated, IRemoteDestroyable, IPlacedOnSurface
    {
        Action<IGem> onCollected { get; set; }

        void Collect();
        void Deactivate();
    }
}
