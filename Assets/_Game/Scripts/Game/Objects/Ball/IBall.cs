﻿using _Game.Scripts.Game.Objects.Interfaces;
using Core.Interfaces;
using Core.Systems.Reset;

namespace _Game.Scripts.Game.Objects.Ball
{
    public interface IBall : IGameObjectHost, IMovable, IDirected, IPlacedOnSurface, IResetable
    {
    }
}
