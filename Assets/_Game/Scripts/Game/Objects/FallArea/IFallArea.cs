﻿using Core.Interfaces;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Game.Objects.FallArea
{
    public interface IFallArea : IGameObjectHost
    {
        GameObject objectMustFall { get; }
        UnityEvent onObjectFallen { get; set; }
    }
}
