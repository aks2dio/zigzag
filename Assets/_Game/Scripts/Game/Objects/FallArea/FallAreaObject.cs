﻿using Core.Extensions;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Game.Objects.FallArea
{
    [RequireComponent(typeof(Collider))]
    class FallAreaObject : MyMonoBehaviour, IFallArea
    {
        #region SERIALIZE_FIELDS
        [SerializeField] private GameObject _ObjectMustFall = null;
        [SerializeField] private UnityEvent _OnObjectFallen = new UnityEvent();
        #endregion // SERIALIZE_FIELDS

        #region PUBLIC_VALUES
        public GameObject objectMustFall => _ObjectMustFall;
        public UnityEvent onObjectFallen {
            get => _OnObjectFallen;
            set => _OnObjectFallen = value;
        }
        #endregion // PUBLIC_VALUES

        ///////////////////////////////////////////////////

        #region MONO_BEHAVIOUR

        protected override void OnChecking()
        {
            CheckField(_ObjectMustFall);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == objectMustFall)
                onObjectFallen?.Invoke();
        }
        
        #endregion // MONO_BEHAVIOUR
    }
}
