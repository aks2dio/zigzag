﻿using System;
using Core.Extensions;
using _Game.Scripts.Game.Objects.Interfaces;

namespace _Game.Scripts.Game.Objects
{
    public class DestroyableMonoBehaviour : MyMonoBehaviour, IRemoteDestroyable
    {
        public Action<IRemoteDestroyable> onBecomeDestroyable { get; set; }

        public void SetDestroyed()
        {
            onBecomeDestroyable?.Invoke(this);
        }
    }
}
