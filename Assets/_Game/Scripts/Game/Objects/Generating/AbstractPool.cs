﻿using Core.Systems.Reset;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Objects.Generating
{
    public abstract class AbstractPool<T> : MemoryPool<T> where T : Component
    {
        private Transform _Parent;

        public AbstractPool(Transform parent)
            => _Parent = parent;

        protected override void OnSpawned(T item)
            => item.gameObject.SetActive(true);

        protected override void OnDespawned(T item)
        {
            item.gameObject.SetActive(false);
            item.transform.SetParent(_Parent);

            if (item is IResetable resetable)
                resetable.Reset();
        }

        protected override void OnCreated(T item)
            => item.gameObject.SetActive(false);
    }
}
