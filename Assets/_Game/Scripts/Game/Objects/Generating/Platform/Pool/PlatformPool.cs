﻿using _Game.Scripts.Game.Objects.Platform;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Pool
{
    class PlatformPool : AbstractPool<PlatformObject>, IPlatformPool
    {
        public PlatformPool(Transform parent) : base(parent)
        { }
    }
}
