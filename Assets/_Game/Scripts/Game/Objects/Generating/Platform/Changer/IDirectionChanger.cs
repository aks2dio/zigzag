﻿using Core.Systems.Reset;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Changer
{
    public interface IDirectionChanger : IResetable
    {
        Vector3 currentDirection { get; }

        Vector3 ChangeDirection(int stepsCompleted);
    }
}
