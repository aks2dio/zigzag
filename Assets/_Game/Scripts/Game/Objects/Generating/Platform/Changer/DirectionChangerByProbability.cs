﻿using _Game.Scripts.Game.Handlers.Direction;
using _Game.Scripts.Game.Handlers.Probability;
using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Changer
{
    class DirectionChangerByProbability : IDirectionChanger
    {
        private IDirectionHandler _directionHandler { get; set; }
        private IProbabilitierByCurve _probabilitier { get; set; }

        public Vector3 currentDirection => _directionHandler.current;

        ///////////////////////////////////////////

        public DirectionChangerByProbability(IDirectionHandler directionHandler, IProbabilitierByCurve probabilitier)
        {
            _directionHandler = directionHandler;
            _probabilitier = probabilitier;
        }

        //////////////////////////////////////////

        public Vector3 ChangeDirection(int stepsCompleted)
        {
            if (_probabilitier.IsSuccess(stepsCompleted))
                _directionHandler.MoveNext();

            return _directionHandler.current;
        }

        public void Reset()
        {
            _directionHandler.Reset();
            _probabilitier.Reset();
        }
    }
}
