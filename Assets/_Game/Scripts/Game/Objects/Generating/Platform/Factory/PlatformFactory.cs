﻿using _Game.Scripts.Game.Objects.Platform;
using Zenject;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Factory
{
    class PlatformFactory : PlaceholderFactory<IPlatform>, IPlatformFactory
    {
    }
}
