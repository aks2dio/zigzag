﻿using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Extensions;
using _Game.Scripts.Game.Objects.Generating.Platform.Calculator;
using _Game.Scripts.Game.Objects.Generating.Platform.Changer;
using _Game.Scripts.Game.Objects.Platform;
using _Game.Scripts.Parameters.Interfaces;
using Core.Extensions;
using Core.Systems.Reset;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Objects.Generating.Platform
{
    public abstract class PlatformManager : MyMonoBehaviour, IResetable
    {
        #region SERIALIZE_FIELDS

        [Header("Attachments")]
        [SerializeField] protected Transform _PlatformsContainer = null;

        [Header("Config")]
        [SerializeField] protected bool _Active = false;

        [Header("Events")]
        [SerializeField] protected IPlatformUnityEvent _OnCreated = new IPlatformUnityEvent();
        [SerializeField] protected IntUnityEvent _OnChangeStep = new IntUnityEvent();

        #endregion // SERIALIZE_FIELDS

        #region PRIVATE_VALUES

        [Inject] protected IPlatformManagerParameters _parameters { get; }
        [Inject] protected IDirectionChanger _direction { get; }
        [Inject] protected PositionCalculator _calculator { get; }

        protected bool _silentMode { get; set; }

        protected SortedDictionary<int, IPlatform> _platformsBySteps { get; }
            = new SortedDictionary<int, IPlatform>();

        protected List<IPlatform> _initialPlatforms { get; }
            = new List<IPlatform>();

        #endregion // PRIVATE_VALUES

        #region PUBLIC_VALUES

        public bool active => _Active;
        public int currentStep { get; private set; }
        public IPlatformUnityEvent onCreated => _OnCreated;

        #endregion // PUBLIC_VALUES

        ///////////////////////////////////////////////////////////

        #region MONO_BEHAVIOUR

        protected override void OnChecking()
        {
            CheckField(_PlatformsContainer);
        }

        private void Start()
            => CacheValues();

        #endregion // MONO_BEHAVIOUR

        #region PUBLIC_METHODS

        public abstract void Init();

        public Vector3 GetInitialPositionOn()
        {
            var result = Vector3.zero;

            foreach (var platform in _initialPlatforms)
                result += platform.gameObject.transform.position;

            return result / _initialPlatforms.Count;
        }

        public void SetActive(bool status)
        {
            _Active = status;
        }

        public virtual void Reset()
        {
            ResetFromCache();
            _direction.Reset();
            _calculator.Reset();
            currentStep = default;
            _silentMode = default;

            foreach (var platform in _platformsBySteps.Values)
                platform.SetDestroyed();

            foreach (var platform in _initialPlatforms)
                platform.SetDestroyed();

            _platformsBySteps.Clear();
            _initialPlatforms.Clear();
        }

        #endregion // PUBLIC_METHODS

        #region CACHING

        private bool _active_Cache { get; set; }
        private void CacheValues()
            => _active_Cache = _Active;
        private void ResetFromCache()
            => _Active = _active_Cache;

        #endregion // CACHING

        #region STEP_OPERATIONS

        private void SetCurrentStep(int currentStep)
        {
            this.currentStep = currentStep;
            OnChangeCurrentStep();
            _OnChangeStep?.Invoke(this.currentStep);
        }

        protected abstract void OnChangeCurrentStep();

        #endregion // STEP_OPERATIONS

        #region BEHIND_VISIBILITY

        protected void DeactivateBehindInitialPlatforms()
        {
            for (var i = 0; i < _initialPlatforms.Count; i++)
                if (_initialPlatforms[i].index < currentStep)
                    _initialPlatforms[i].Deactivate();
        }

        protected void CorrectPlatforms()
        {
            var minCalculatedStep = _calculator.calculatedSteps.FirstOrDefault();
            var minVisibleSteps = currentStep - _parameters.stepThresholdBack;

            if (minVisibleSteps <= minCalculatedStep)
                return;

            DestroyOldPlatforms(minCalculatedStep, minVisibleSteps);
        }

        protected void DestroyOldPlatforms(int minCalculatedStep, int minVisibleSteps)
        {
            for (var step = minCalculatedStep; step < minVisibleSteps; step++)
            {
                if (_platformsBySteps.TryGetValue(step, out var platform))
                    platform.SetDestroyed();

                _platformsBySteps.Remove(step);
                _calculator.ClearStep(step);
            }

            for (var i = _initialPlatforms.Count - 1; i >= 0; i--)
            {
                if (_initialPlatforms[i].index >= minVisibleSteps)
                    continue;

                _initialPlatforms[i].SetDestroyed();
                _initialPlatforms.RemoveAt(i);
            }
        }

        #endregion // BEHIND_VISIBILITY

        #region HANDLING_PLATFORMS_EVENTS
        protected IPlatform ConfigurePlatform(IPlatform platform, Vector3 position, int index)
        {
            var plTransform = platform.gameObject.transform;
            plTransform.SetParent(_PlatformsContainer);
            plTransform.localPosition = position;

            platform.index = index;
            platform.Appear(active && !_silentMode);
            platform.onStartBeUsing += OnBecomeUsing;

            if (!_silentMode)
                onCreated?.Invoke(platform);

            return platform;
        }

        private void OnBecomeUsing(IPlatform platform) =>
            SetCurrentStep(platform.index);

        #endregion // HANDLING_PLATFORMS_EVENTS
    }
}
