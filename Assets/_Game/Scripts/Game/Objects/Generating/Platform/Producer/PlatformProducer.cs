﻿using _Game.Scripts.Game.Objects.Generating.Platform.Factory;
using _Game.Scripts.Game.Objects.Platform;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Producer
{
    class PlatformProducer : AbstractProducer<IPlatform>, IPlatformProducer
    {
        private readonly IPlatformFactory _factory;

        public PlatformProducer(IPlatformFactory factory)
            => _factory = factory;

        protected override IPlatform CreateEntity()
            => _factory.Create();
        protected override void HandleUnusedEntity(IPlatform entity)
            => UnityEngine.Object.Destroy(entity.gameObject);
    }
}
