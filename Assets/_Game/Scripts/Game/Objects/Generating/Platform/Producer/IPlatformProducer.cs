﻿using _Game.Scripts.Game.Objects.Interfaces;
using _Game.Scripts.Game.Objects.Platform;

namespace _Game.Scripts.Game.Objects.Generating.Platform.Producer
{
    interface IPlatformProducer : IProducer<IPlatform>
    {
    }
}
