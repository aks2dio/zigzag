﻿using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Positioning
{
    class InSquarePositionNoiser : IPositionNoiser
    {
        public readonly float squareSide;
        public readonly Vector3 mask;

        //////////////////////////////////////////////////////////////

        public InSquarePositionNoiser(float squareSide, Vector3 mask)
        {
            this.squareSide = squareSide;
            this.mask = mask;
        }

        //////////////////////////////////////////////////////////////
        
        public Vector3 NoisePosition(Vector3 initialPosition)
        {
            var mask = _correctedMask;

            return initialPosition + new Vector3(
                GetMaskedOffset(mask.x),
                GetMaskedOffset(mask.y),
                GetMaskedOffset(mask.z));
        }

        //////////////////////////////////////////////////////////////

        private float GetMaskedOffset(float mask) =>
            mask > 0 ? _randomOffset : 0;

        private float _randomOffset {
            get {
                var offset = squareSide / 2f;
                return Random.Range(-offset, offset);
            }
        }

        private Vector3 _correctedMask =>
            new Vector3(
                mask.x == 0 ? 0 : 1,
                mask.y == 0 ? 0 : 1,
                mask.z == 0 ? 0 : 1);
    }
}
