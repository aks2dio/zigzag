﻿using UnityEngine;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Positioning
{
    public interface IPositionNoiser
    {
        Vector3 NoisePosition(Vector3 initialPosition);
    }
}
