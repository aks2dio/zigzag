﻿using _Game.Scripts.Game.Objects.Gem;
using Zenject;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Pool
{
    interface IGemPool : IMemoryPool<GemObject>
    {
    }
}
