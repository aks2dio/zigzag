﻿using System.Linq;
using _Game.Scripts.Extensions;
using _Game.Scripts.Game.Objects.Gem;
using _Game.Scripts.Game.Objects.Interfaces;
using _Game.Scripts.Game.Objects.Platform;
using Core.Extensions;
using Core.Systems.Reset;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Game.Objects.Generating.Gem
{
    public abstract class GemManager : MyMonoBehaviour, IResetable
    {
        [Header("Attachments")]
        [SerializeField] protected Transform _Container = null;

        [Header("Events")]
        [SerializeField] protected UnityEvent _OnCollected = new UnityEvent();

        protected SafeHashSet<IGem> _gems { get; } = new SafeHashSet<IGem>();

        public UnityEvent onCollected => _OnCollected;

        ////////////////////////////////////////////

        protected override void OnChecking() =>
            CheckField(_Container);


        public abstract void Init();

        public virtual void Reset()
        {
            foreach (var gem in _gems.ToArray())
                gem.SetDestroyed();

            _gems.Clear();
        }

        protected IGem ConfigureGem(IGem gem, Vector3 position, IPlatform platform)
        {
            gem.gameObject.transform.SetParent(_Container);
            gem.SetPositionOnSurface(position);

            gem.onCollected += OnCollected;
            gem.onBecomeDestroyable += OnGemBecomeDestroyable;

            Subscribe();

            return gem;

            #region INNER_METHODS

            void Subscribe()
            {
                platform.onDeactivating += OnPlatformDeactivating;
                platform.onBecomeDestroyable += OnPlatformBecomeDestroyable;
            }

            void Unsubscribe()
            {
                platform.onDeactivating -= OnPlatformDeactivating;
                platform.onBecomeDestroyable -= OnPlatformBecomeDestroyable;
            }

            void OnCollected(IGem g)
            {
                onCollected?.Invoke();
                Unsubscribe();
            }

            void OnGemBecomeDestroyable(IRemoteDestroyable g)
            {
                g.onBecomeDestroyable -= OnGemBecomeDestroyable;
                _gems.Remove(gem);
                Unsubscribe();
            }

            void OnPlatformBecomeDestroyable(IRemoteDestroyable p)
            {
                gem.SetDestroyed();
                Unsubscribe();
            }

            void OnPlatformDeactivating()
            {
                gem.Deactivate();
                Unsubscribe();
            }

            #endregion // INNER_METHODS
        }
    }
}
