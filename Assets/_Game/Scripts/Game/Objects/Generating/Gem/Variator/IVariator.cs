﻿using Core.Systems.Reset;

namespace _Game.Scripts.Game.Objects.Generating.Gem.Variator
{
    public interface IVariator : IResetable
    {
        bool IsSuccess();
    }
}
