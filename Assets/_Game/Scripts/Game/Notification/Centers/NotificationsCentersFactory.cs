﻿using _Game.Scripts.Game.Localization;
using Core;

namespace _Game.Scripts.Game.Notification.Centers
{
    public class NotificationsCentersFactory : LoggedClass
    {
        private readonly LocalizationManager _localizationManager;
        private readonly NotificationsChannelParameters _channelParameters;


        public NotificationsCentersFactory(LocalizationManager localizationManager, NotificationsChannelParameters channelParameters)
        {
            _channelParameters = channelParameters;
            _localizationManager = localizationManager;
        }


        public NotificationsCenter Create(bool useLocalization)
        {
#if UNITY_ANDROID
            Log($"Create {nameof(AndroidNotificationsCenter)}");
            return useLocalization ?
                new AndroidNotificationsCenter(_localizationManager, _channelParameters) :
                new AndroidNotificationsCenter(_channelParameters);
#endif
            Log($"Create {nameof(EmptyNotificationsCenter)}");
            return useLocalization ?
                new EmptyNotificationsCenter(_localizationManager, _channelParameters) :
                new EmptyNotificationsCenter(_channelParameters);
        }
    }
}
