﻿using System;
using _Game.Scripts.Game.Localization;
using Unity.Notifications.Android;

namespace _Game.Scripts.Game.Notification.Centers
{
    public class AndroidNotificationsCenter : NotificationsCenter
    {
        private AndroidNotificationChannel _channel;


        public AndroidNotificationsCenter(LocalizationManager localizationManager, NotificationsChannelParameters channelParameters)
            : base(localizationManager, channelParameters)
        { }

        public AndroidNotificationsCenter(NotificationsChannelParameters channelParameters) : base(channelParameters)
        { }


        public override void CancelAllNotifications()
        {
            Log("Cancel all notifications");
            AndroidNotificationCenter.CancelAllNotifications();
        }

        public override void ScheduleNotification(NotificationConfig notificationConfig)
        {
            Log($"Scheduled {notificationConfig.title}");

            var notification = new AndroidNotification
            {
                Title = HandleStringData(notificationConfig.title),
                Text = HandleStringData(notificationConfig.body),
                LargeIcon = notificationConfig.largeIcon,
                SmallIcon = notificationConfig.smallIcon,
                FireTime = DateTime.Now + notificationConfig.timeOffset
            };

            AndroidNotificationCenter.SendNotification(notification, _channel.Id);
        }


        protected override void InitChannel(NotificationsChannelParameters channelParameters)
        {
            Log($"Init channel {channelParameters.name}");

            _channel = new AndroidNotificationChannel()
            {
                Id = channelParameters.id,
                Name = channelParameters.name,
                Importance = channelParameters.Importance,
                Description = channelParameters.description
            };

            AndroidNotificationCenter.RegisterNotificationChannel(_channel);
        }
    }
}
