﻿using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Notification.Centers;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Notification
{
    public class NotificationsManager : LoggedMonoBehaviour
    {
        [Inject] private SettingsData _SettingsData { get; }
        [Inject] private NotificationParameters _Parameters { get; }
        [Inject] private NotificationsCenter _Center { get; }


        private void Start()
        {
            _SettingsData.onNotificationsChanged += UpdateStatus;
            UpdateStatus();
        }

        private void OnDestroy()
        {
            _SettingsData.onNotificationsChanged -= UpdateStatus;
        }


        private void UpdateStatus()
        {
            _Center.CancelAllNotifications();

            if (!_SettingsData.notifications)
                return;

            foreach (var n in _Parameters.notifications)
                _Center.ScheduleNotification(n);
        }
    }
}
