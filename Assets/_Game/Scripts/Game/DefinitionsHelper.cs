﻿namespace _Game.Scripts.Game
{
    internal static class DefinitionsHelper
    {
        public const string Parameters = "ScriptableObjects/";
        public const string Installers = "Installers/";
        public const string Skins = "Skins/";
        public const string Sound = "Sound/";
    }
}
