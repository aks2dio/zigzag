﻿using Core.Extensions;
using UnityEngine;
using UnityEngine.Networking;

namespace _Game.Scripts.Game.EmailSending
{
    class EmailSender : MyMonoBehaviour
    {
        [SerializeField] private EmailSenderSettings _SupportSettings = null;

        /////////////////////////////////////

        protected override void OnChecking() => 
            CheckField(_SupportSettings);

        /////////////////////////////////////

        [ContextMenu("Send support email")]
        public void SendSupportEmail()
        {
            var email = _SupportSettings.sender;
            var subject = MyEscapeURL(_SupportSettings.subject);
            var body = MyEscapeURL(_SupportSettings.body);

            Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
        }

        /////////////////////////////////////

        private string MyEscapeURL(string url) => 
            UnityWebRequest.EscapeURL(url).Replace("+", "%20");
    }
}
