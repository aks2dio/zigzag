﻿using System;
using _Game.Scripts.Instruments.Counter;

namespace _Game.Scripts.Game.Scoring
{
    class ScoreInner : IScoreInner
    {
        #region PRIVATE_FIELDS
        
        private ICounter<int> _counter { get; }
        
        #endregion // PRIVATE_FIELDS

        #region PUBLIC_FIELDS
        
        public int current => _counter.current;
        public Action<int> onChanged { get; set; }
        
        #endregion // PUBLIC_FIELDS

        /////////////////////////////////////////////////

        #region CONSTRUCTORS
        
        public ScoreInner(ICounter<int> counter)
        {
            _counter = counter;
            _counter.onChanged += count => onChanged?.Invoke(count);
        }
        
        #endregion // CONSTRUCTORS

        /////////////////////////////////////////////////

        #region PUBLIC_METHODS
        
        public void Add()
            => Add(1);

        public void Add(int count)
        {
            for (var i = 0; i < count; i++)
                _counter.Increase();
        }

        public void Substract()
            => Substract(1);

        public void Substract(int count)
        {
            for (var i = 0; i < count; i++)
                _counter.Dicrease();
        }

        public void Reset()
            => _counter.Reset();
        
        #endregion // PUBLIC_METHODS
    }
}
