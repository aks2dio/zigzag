﻿using Core.Components;
using Core.Extensions;
using UnityEngine;

namespace _Game.Scripts.Game.Scoring.Displayer
{
    class ScoreDisplayer : MyMonoBehaviour, IScoreDisplayer
    {
        #region SERIALIZE_FIELDS
        
        [SerializeField] private CompositeText _ScoreText = null;
        [SerializeField] private CompositeText _GemsText = null;
        
        #endregion // SERIALIZE_FIELDS

        ///////////////////////////////////////////////////////////

        #region CHECKING
        
        protected override void OnChecking()
        {
            CheckField(_ScoreText);
            CheckField(_GemsText);
        }
        
        #endregion // CHECKING

        #region PUBLIC_METHODS
        
        public void SetScore(int score)
            => _ScoreText.SetPart(score, 1);

        public void SetGems(int gems)
            => _GemsText.SetPart(gems, 1);
        
        #endregion // PUBLIC_METHODS
    }
}
