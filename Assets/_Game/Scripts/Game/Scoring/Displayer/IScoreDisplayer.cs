﻿namespace _Game.Scripts.Game.Scoring.Displayer
{
    public interface IScoreDisplayer
    {
        void SetScore(int score);
        void SetGems(int gems);
    }
}
