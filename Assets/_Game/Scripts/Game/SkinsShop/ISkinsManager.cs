﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop
{
    interface ISkinsManager
    {
        event Action onChangeSelected;
        event Action<int> onChangeSelectedInGroup;

        bool BuyItem(int groupId, int itemId);
        bool SelectItem(int groupId, int itemId);

        bool IsBought(int groupId, int itemId);
        bool IsSelected(int groupId, int itemId);

        bool TryGetGroupId(string groupName, out int id);
        bool TryGetSelectedContent(int groupId, out ScriptableObject content);
    }
}
