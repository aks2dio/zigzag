﻿using _Game.Scripts.AssetsLoading;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.SkinsShop.SkinSetting
{
    abstract class SkinSetter<T> : MonoBehaviour where T : ScriptableObject
    {
        [Inject] private ISkinsManager _skinsManager;

        [SerializeField] private int _GroupId = -1;

        protected abstract IUnloadable[] _unloadableLoaders { get; }

        /////////////////////////////////////////////////////////

        private void Start()
        {
            _skinsManager.onChangeSelectedInGroup += UpdateSkinIfMyGroupId;
            UpdateSkin();
        }

        private void OnDestroy()
        {
            for(var i = 0; i < _unloadableLoaders.Length; i++)
                _unloadableLoaders[i].Unload();

            _skinsManager.onChangeSelectedInGroup -= UpdateSkinIfMyGroupId;
        }

        /////////////////////////////////////////////////////////

        protected abstract void UpdateSkin();

        /////////////////////////////////////////////////////////

        protected bool TryGetContent(out T content)
        {
            content = null;

            if (_skinsManager.TryGetSelectedContent(_GroupId, out var selectedContent) == false)
                return false;

            if (selectedContent is T == false)
                return false;

            content = (T) selectedContent;
            return true;
        }

        /////////////////////////////////////////////////////////

        private void UpdateSkinIfMyGroupId(int groupId)
        {
            if (_GroupId != groupId)
                return;

            UpdateSkin();
        }
    }
}
