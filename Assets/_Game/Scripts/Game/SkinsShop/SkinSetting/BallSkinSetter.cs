using _Game.Scripts.AssetsLoading;
using _Game.Scripts.Game.SkinsShop.Data.SerializeObjects;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.SkinSetting
{
    class BallSkinSetter : SkinSetter<BallSkin>
    {
        private readonly AssetLoaderByReference<Material> _materialLoader = new AssetLoaderByReference<Material>();

        protected override IUnloadable[] _unloadableLoaders => new IUnloadable[] {_materialLoader};

        //////////////////////////////////////

        protected override async void UpdateSkin()
        {
            if (TryGetComponent<MeshRenderer>(out var meshRenderer) == false)
                return;

            if (TryGetContent(out var content) == false)
                return;

            var material = await _materialLoader.Load(content.materialReference);
            meshRenderer.material = material;
        }
    }
}
