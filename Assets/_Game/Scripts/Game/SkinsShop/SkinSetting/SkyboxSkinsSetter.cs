using _Game.Scripts.AssetsLoading;
using _Game.Scripts.Game.SkinsShop.Data.SerializeObjects;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.SkinSetting
{
    [RequireComponent(typeof(Camera))]
    class SkyboxSkinsSetter : SkinSetter<BackgroundSkin>
    {
        private readonly AssetLoaderByReference<Material> _materialLoader = new AssetLoaderByReference<Material>();

        protected override IUnloadable[] _unloadableLoaders => new IUnloadable[] {_materialLoader};

        //////////////////////////////////////

        protected override async void UpdateSkin()
        {
            if (TryGetContent(out var content) == false)
                return;

            if (TryGetComponent<Camera>(out var cam) == false)
                return;

            if (content.materialReference != null)
            {
                cam.clearFlags = CameraClearFlags.Skybox;

                var material = await _materialLoader.Load(content.materialReference);
                RenderSettings.skybox = material;
            }
            else
            {
                cam.clearFlags = CameraClearFlags.SolidColor;
                cam.backgroundColor = content.cameraColor;
            }
        }
    }
}