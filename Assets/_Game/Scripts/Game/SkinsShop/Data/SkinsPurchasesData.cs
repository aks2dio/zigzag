﻿using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Game.Data;
using _Game.Scripts.Game.Data.Save;
using _Game.Scripts.Game.SkinsShop.Save;

namespace _Game.Scripts.Game.SkinsShop.Data
{
    public class SkinsPurchasesData : DataObject<SaveSkinsPurchasesData>
    {
        private Dictionary<int, HashSet<int>> _purchasedItemsByGroups { get; set; }

        protected override string _save_key { get; } = "skins_purchases_data";

        /////////////////////////////////////////////////////

        public SkinsPurchasesData(SaveManager saveManager) : base(saveManager)
        { }

        /////////////////////////////////////////////////////

        public IEnumerable<int> GetBoughtItems(int groupId)
            => _purchasedItemsByGroups.TryGetValue(groupId, out var result) ?
            result : new HashSet<int>();

        public bool IsBought(int groupId, int idItem)
        {
            foreach (var item in GetBoughtItems(groupId))
                if (item == idItem)
                    return true;

            return false;
        }

        public void SetItemAsBought(int groupId, int itemId)
        {
            if (IsBought(groupId, itemId))
                return;

            if (_purchasedItemsByGroups.ContainsKey(groupId) == false)
                _purchasedItemsByGroups[groupId] = new HashSet<int>();

            _purchasedItemsByGroups[groupId].Add(itemId);
            InvokeOnChanged();

            Save();
        }

        /////////////////////////////////////////////////////

        public override SaveSkinsPurchasesData CreateSaveData() =>
            new SaveSkinsPurchasesData()
            {
                groupedItems = ConvertToSaveFormat(_purchasedItemsByGroups)
            };

        protected override void OnApply(SaveSkinsPurchasesData saveData, SaveApplyType applyType)
        {
            var saveDictionary = ConvertFromSaveFormat(saveData.groupedItems);

            _purchasedItemsByGroups =
                _purchasedItemsByGroups == null || applyType == SaveApplyType.Override
                ? saveDictionary
                : ResultDataToApply(saveDictionary, _purchasedItemsByGroups, applyType);
        }

        /////////////////////////////////////////////////////

        private SkinsShopInGroupPurchases[] ConvertToSaveFormat(Dictionary<int, HashSet<int>> originalFormat)
        {
            if (originalFormat == null)
                return new SkinsShopInGroupPurchases[0];

            return originalFormat
            .Select(x => new SkinsShopInGroupPurchases(x.Key, x.Value.ToArray())).ToArray();
        }

        private Dictionary<int, HashSet<int>> ConvertFromSaveFormat(SkinsShopInGroupPurchases[] saveFormat)
        {
            if (saveFormat == null)
                return new Dictionary<int, HashSet<int>>();

            return saveFormat
            .ToDictionary(x => x.groupId, x => new HashSet<int>(x.itemIds));
        }
    }
}
