﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Scripts.Game.SkinsShop.Data.SerializeObjects
{
    [CreateAssetMenu(fileName = nameof(GemSkin), menuName = DefinitionsHelper.Skins + nameof(GemSkin))]
    class GemSkin : ScriptableObject
    {
        [SerializeField] private AssetReference _MaterialReference = null;

        public AssetReference materialReference => _MaterialReference;
    }
}
