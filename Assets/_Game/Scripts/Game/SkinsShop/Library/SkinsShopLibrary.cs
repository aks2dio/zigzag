﻿using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.Library
{
    [CreateAssetMenu(fileName = nameof(SkinsShopLibrary), menuName = DefinitionsHelper.Parameters + nameof(SkinsShopLibrary))]
    class SkinsShopLibrary : ScriptableObject, ISkinsShopLibrary
    {
        [SerializeField] private SkinsShopGroup[] _ItemsGroups = new SkinsShopGroup[0];

        public IEnumerable<SkinsShopGroup> itemsGroups => _ItemsGroups;

        ///////////////////////////////////////////////////////

        public bool ContainsGroup(int groupId)
        {
            foreach (var ig in _ItemsGroups)
                if (ig.id == groupId)
                    return true;

            return false;
        }

        public bool ContainsItem(int groupId, int itemId)
        {
            foreach (var i in GetItemsInGroup(groupId))
                if (i.id == itemId)
                    return true;

            return false;
        }

        public SkinsShopItem[] GetItemsInGroup(int groupId)
        {
            foreach (var ig in _ItemsGroups)
                if (ig.id == groupId)
                    return ig.items;

            return new SkinsShopItem[0];
        }

        public bool TryGetItem(int groupId, int itemId, out SkinsShopItem item)
        {
            foreach (var i in GetItemsInGroup(groupId))
                if (i.id == itemId)
                {
                    item = i;
                    return true;
                }

            item = default;
            return false;
        }

        public bool TryGetItemsInGroup(int groupId, out SkinsShopItem[] items)
        {
            items = GetItemsInGroup(groupId);
            return items.Length > 0;
        }
    }
}
