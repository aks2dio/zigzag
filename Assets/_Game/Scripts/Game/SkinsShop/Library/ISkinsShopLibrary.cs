﻿using System.Collections.Generic;

namespace _Game.Scripts.Game.SkinsShop.Library
{
    interface ISkinsShopLibrary
    {
        IEnumerable<SkinsShopGroup> itemsGroups { get; }

        bool ContainsGroup(int groupId);
        bool ContainsItem(int groupId, int itemId);
        SkinsShopItem[] GetItemsInGroup(int groupId);
        bool TryGetItemsInGroup(int groupId, out SkinsShopItem[] items);
        bool TryGetItem(int groupId, int itemId, out SkinsShopItem item);
    }
}
