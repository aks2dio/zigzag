﻿using System;
using _Game.Scripts.Game.Data;

namespace _Game.Scripts.Game.SkinsShop.Save
{
    [Serializable]
    public class SaveSkinsSelectedData : ISaveData
    {
        public SkinsShopInGroupSelections[] groupedItems;
    }
}
