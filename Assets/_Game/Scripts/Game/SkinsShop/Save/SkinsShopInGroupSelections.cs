﻿using System;

namespace _Game.Scripts.Game.SkinsShop.Save
{
    [Serializable]
    public struct SkinsShopInGroupSelections
    {
        public int groupId;
        public int itemId;

        public SkinsShopInGroupSelections(int groupId, int itemId)
        {
            this.groupId = groupId;
            this.itemId = itemId;
        }
    }
}
