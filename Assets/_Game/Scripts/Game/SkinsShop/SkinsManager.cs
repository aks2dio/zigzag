﻿using System;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Data.Purchasing;
using _Game.Scripts.Game.SkinsShop.Data;
using _Game.Scripts.Game.SkinsShop.Library;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop
{
    class SkinsManager : ISkinsManager
    {
        private readonly IAnalyticsManager _analytics;
        private readonly IBuyingManager _purchaseManager;
        private readonly ISkinsShopLibrary _skinsLibrary;
        private readonly SkinsPurchasesData _purchasesData;
        private readonly SkinsSelectedData _selectedData;

        public event Action onChangeSelected {
            add => _selectedData.onChanged += value;
            remove => _selectedData.onChanged -= value;
        }

        public event Action<int> onChangeSelectedInGroup {
            add => _selectedData.onChangeSelectedInGroup += value;
            remove => _selectedData.onChangeSelectedInGroup -= value;
        }

        //////////////////////////////////////////////////////

        public SkinsManager(ISkinsShopLibrary skinsLibrary, IBuyingManager purchaseManager, 
            SkinsPurchasesData purchasesData, SkinsSelectedData selectedData, IAnalyticsManager analytics)
        {
            _analytics = analytics;
            _purchaseManager = purchaseManager;
            _purchasesData = purchasesData;
            _selectedData = selectedData;
            _skinsLibrary = skinsLibrary;
        }

        //////////////////////////////////////////////////////

        public bool BuyItem(int groupId, int itemId)
        {
            if (IsBought(groupId, itemId))
                return false;

            if (_skinsLibrary.TryGetItem(groupId, itemId, out var item) == false)
                return false;

            if (_purchaseManager.Buy(item) == false)
                return false;

            _analytics.UnlockItem(AnalyticsHelper.GetItemEventData(groupId, itemId));
             _purchasesData.SetItemAsBought(groupId, itemId);
            return true;
        }

        public bool SelectItem(int groupId, int itemId)
        {
            if (IsBought(groupId, itemId) == false)
                return false;

            if (IsSelected(groupId, itemId))
                return false;

            _analytics.SelectItem(AnalyticsHelper.GetItemEventData(groupId, itemId));
            _selectedData.SetItemAsSelected(groupId, itemId);
            return true;
        }

        //////////////////////////////////////////////////////

        public bool IsBought(int groupId, int itemId)
        {
            if (_purchasesData.IsBought(groupId, itemId))
                return true;

            return _skinsLibrary.TryGetItem(groupId, itemId, out var item) && 
                item.availableByDefault;
        }

        public bool IsSelected(int groupId, int itemId)
        {
            if (_selectedData.IsSelected(groupId, itemId))
                return true;

            // если уже выбран какой-то другой элемент
            if (_selectedData.TryGetSelectedItem(groupId, out var selectedId))
                return false;

            if (_skinsLibrary.TryGetItem(groupId, itemId, out var item) == false)
                return false;

            return item.selectedByDefault;
        }

        //////////////////////////////////////////////////////

        public bool TryGetGroupId(string groupName, out int id)
        {
            id = -1;
            
            foreach (var it in _skinsLibrary.itemsGroups)
                if (it.header == groupName)
                {
                    id = it.id;
                    break;
                }

            return id != -1;
        }

        //////////////////////////////////////////////////////

        public bool TryGetSelectedContent(int groupId, out ScriptableObject content)
        {
            content = null;

            if (_selectedData.TryGetSelectedItem(groupId, out var itemId) == false)
            {
                foreach (var i in _skinsLibrary.GetItemsInGroup(groupId))
                    if (i.selectedByDefault)
                        return content = i.content;
            }

            if (_skinsLibrary.TryGetItem(groupId, itemId, out var item))
                content = item.content;

            return content;
        }
    }
}
