﻿using _Game.Scripts.Game.Objects.Interfaces;

namespace _Game.Scripts.Game.SkinsShop.View.Group
{
    interface ISkinsShopItemGroupViewFactory : IFactory<ISkinsShopItemGroupView>
    {
    }
}
