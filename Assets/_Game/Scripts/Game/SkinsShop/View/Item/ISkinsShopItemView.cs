﻿using System;
using _Game.Scripts.Game.SkinsShop.Library;
using _Game.Scripts.Game.SkinsShop.View.Group;
using Core.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    interface ISkinsShopItemView : IGameObjectHost
    {
        bool bought { get; }
        SkinsShopItem data { get; }
        ISkinsShopItemGroupView group { get; }
        Action<ISkinsShopItemView> onClicked { get; set; }

        void Construct(Transform container, SkinsShopItem data, ISkinsShopItemGroupView group);

        void SetBought(bool status);
        void SetSelected(bool status);
    }
}
