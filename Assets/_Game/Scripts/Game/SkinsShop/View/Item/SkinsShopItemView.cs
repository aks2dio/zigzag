﻿using System;
using System.Linq;
using _Game.Scripts.Game.SkinsShop.Library;
using _Game.Scripts.Game.SkinsShop.View.Group;
using _Game.Scripts.UI.Elements;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(SelectableElement))]
    class SkinsShopItemView : MyMonoBehaviour, ISkinsShopItemView
    {
        private Button _buttonComponent;
        private SelectableElement _selectableElement;

        private ISkinsShopItemBoughtView _BoughtView { get; set; }
        private ISkinsShopItemSellableView _SellableView { get; set; }

        public bool bought { get; private set; }
        public SkinsShopItem data { get; private set; }
        public ISkinsShopItemGroupView group { get; private set; }

        public Action<ISkinsShopItemView> onClicked { get; set; }

        //////////////////////////////////////////////////////

        public void Construct(Transform container, SkinsShopItem data, ISkinsShopItemGroupView containerView)
        {
            this.data = data;
            this.group = containerView;

            SetBought(bought);
            _SellableView.SetPrice(data.price);

            transform.SetParent(container);

            _BoughtView.SetIcon(data.iconReference, data.color);
        }

        //////////////////////////////////////////////////////

        private void OnEnable()
        {
            Attach(ref _buttonComponent);
            Attach(ref _selectableElement);

            _buttonComponent.onClick.AddListener(OnClickAction);

            if (_BoughtView != null && _SellableView != null)
                return;

            var views = GetComponentsInChildren<ISkinsShopItemInnerView>(true);
            _BoughtView = views.FirstOrDefault(v => v is ISkinsShopItemBoughtView) as ISkinsShopItemBoughtView;
            _SellableView = views.FirstOrDefault(v => v is ISkinsShopItemSellableView) as ISkinsShopItemSellableView;
        }

        private void OnDisable()
        {
            _BoughtView.SetIcon(null);
            _buttonComponent.onClick.RemoveListener(OnClickAction);
        }

        private void OnClickAction() => onClicked?.Invoke(this);

        //////////////////////////////////////////////////////

        public void SetBought(bool status)
        {
            bought = status;
            SetView(status);
        }

        public void SetSelected(bool status)
            => _selectableElement.SetSelected(status);

        private void SetView(bool bought)
        {
            _BoughtView.SetEnabled(bought);
            _SellableView.SetEnabled(!bought);
        }
    }
}
