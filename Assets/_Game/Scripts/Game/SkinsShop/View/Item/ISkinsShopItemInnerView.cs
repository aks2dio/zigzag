﻿using Core.Interfaces;

namespace _Game.Scripts.Game.SkinsShop.View.Item
{
    interface ISkinsShopItemInnerView : IGameObjectHost
    {
        void SetEnabled(bool status);
    }
}
