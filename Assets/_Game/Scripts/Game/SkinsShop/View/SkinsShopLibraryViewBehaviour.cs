﻿using _Game.Scripts.Game.SkinsShop.View.Item;
using _Game.Scripts.Game.Toast;
using Core.Extensions;
using Zenject;

namespace _Game.Scripts.Game.SkinsShop.View
{
    class SkinsShopLibraryViewBehaviour : MyMonoBehaviour
    {
        [Inject] private ISkinsManager _SkinsManager { get; }
        [Inject] private ToastsManager _ToastsManager { get; }
        [Inject] private ISkinsShopLibraryView _LibraryView { get; }


        ///////////////////////////////////////////////////////////////

        private void OnEnable()
        {
            _LibraryView.onItemClicked += OnItemClicked;
            _LibraryView.Initialize();
        }

        private void OnDisable()
        {
            _LibraryView.onItemClicked -= OnItemClicked;
            _LibraryView.Deinitialize();
        }

        ///////////////////////////////////////////////////////////////

        private void OnItemClicked(ISkinsShopItemView item)
        {
            if (item.bought)
                SelectItem(item);
            else
                BuyItem(item);

            UnityEngine.Debug.Log("Clicked");
        }

        private void SelectItem(ISkinsShopItemView item)
        {
            foreach (var i in item.group.GetItems())
                i.SetSelected(false);

            item.SetSelected(true);
            _SkinsManager.SelectItem(item.group.groupId, item.data.id);
        }

        private void BuyItem(ISkinsShopItemView item)
        {
            if (_SkinsManager.BuyItem(item.group.groupId, item.data.id) == false)
            {
                _ToastsManager.ShowToast(ToastType.NotEnoughGems);
                return;
            }

            item.SetBought(true);
            SelectItem(item);
        }
    }
}
