﻿using Zenject;

namespace _Game.Scripts.Game.SkinsShop.View.Header
{
    class SkinsShopHeaderViewFactory : PlaceholderFactory<ISkinsShopHeaderView>, ISkinsShopHeaderViewFactory
    {
    }
}
