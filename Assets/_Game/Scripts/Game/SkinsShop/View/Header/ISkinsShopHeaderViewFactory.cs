﻿using Zenject;

namespace _Game.Scripts.Game.SkinsShop.View.Header
{
    interface ISkinsShopHeaderViewFactory : IFactory<ISkinsShopHeaderView>
    {
    }
}
