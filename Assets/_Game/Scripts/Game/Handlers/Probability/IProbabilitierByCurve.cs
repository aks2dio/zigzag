﻿using Core.Systems.Reset;
using UnityEngine;

namespace _Game.Scripts.Game.Handlers.Probability
{
    interface IProbabilitierByCurve : IResetable
    {
        AnimationCurve successProbability { get; }

        bool IsSuccess(int stepsCompleted);
    }
}
