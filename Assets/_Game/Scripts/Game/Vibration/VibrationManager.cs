﻿using Core;
using Core.Modules;

namespace _Game.Scripts.Game.Vibration
{
    public class VibrationManager : LoggedClass
    {
        private bool _enabled;
        private IVibrator _vibrator;
        private VibrationSettings _settings;

        public bool enabled
        {
            get => _enabled;
            set
            {
                if (_enabled == value)
                    return;

                _enabled = value;
                Log($"Set enabled: {value}");
            }
        }
        
        ////////////////////////////////////////////

        public VibrationManager(IVibrator vibrator, VibrationSettings vibrationSettings)
        {
            _vibrator = vibrator;
            _settings = vibrationSettings;
        }

        ////////////////////////////////////////////
        
        public void VibrateOnLose()
        {
            if (enabled == false)
                return;
            
            _vibrator.Vibrate(_settings.loseDuration);
            Log("Vibrate on lose");
        }

        public void VibrateOnCollect()
        {
            if (enabled == false)
                return;
            
            _vibrator.Vibrate(_settings.collectDuration);
            Log("Vibrate on collect");
        }
    }
}