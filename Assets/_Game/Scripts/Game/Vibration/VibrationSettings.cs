﻿using UnityEngine;

namespace _Game.Scripts.Game.Vibration
{
    [CreateAssetMenu(fileName = nameof(VibrationSettings), menuName = DefinitionsHelper.Parameters + nameof(VibrationSettings))]
    public class VibrationSettings : ScriptableObject
    {
        [SerializeField] private int _MillisecondsForLose;
        [SerializeField] private int _MillisecondsForCollect;

        public int loseDuration => _MillisecondsForLose;
        public int collectDuration => _MillisecondsForCollect;
    }
}