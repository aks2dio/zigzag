﻿using System;
using _Game.Scripts.Game.Data;
using UnityEngine;

namespace _Game.Scripts.Game.Statistics
{
    [Serializable]
    public class SaveStatisticsData : ISaveData
    {
        private const int _DEFAULT_VALUE = -1;

        public int totalScore = _DEFAULT_VALUE;
        public int maxScoreInGame = _DEFAULT_VALUE;
        public int minScoreInGame = _DEFAULT_VALUE;

        public int totalDistance = _DEFAULT_VALUE;
        public int minDistanceInGame = _DEFAULT_VALUE;
        public int maxDistanceInGame = _DEFAULT_VALUE;

        public int totalGems = _DEFAULT_VALUE;
        public int minGemsInGame = _DEFAULT_VALUE;
        public int maxGemsInGame = _DEFAULT_VALUE;

        public int totalTurns = _DEFAULT_VALUE;
        public int minTurnsInGame = _DEFAULT_VALUE;
        public int maxTurnsInGame = _DEFAULT_VALUE;

        public int maxTurnsOnPlatform = _DEFAULT_VALUE;

        public int gamesPlayed = _DEFAULT_VALUE;

        public int minContinuousDaysPlayed = _DEFAULT_VALUE;
        public int maxContinuousDaysPlayed = _DEFAULT_VALUE;
        public int continuousDaysPlayed = _DEFAULT_VALUE;
        [SerializeField] private string lastDaySessionString = DateTime.MinValue.ToString();

        public DateTime lastDaySession
        {
            get => Convert.ToDateTime(lastDaySessionString);
            set => lastDaySessionString = value.ToString();
        }

        public int purchases = _DEFAULT_VALUE;
        public int skinsBought = _DEFAULT_VALUE;

        public int rewardedViewed = _DEFAULT_VALUE;
        public bool disabledAds;

        public bool readPrivacyPolicy;
        public bool readTermsofUse;
        public bool readPartnersData;

        public bool viewVk;
        public bool viewLinkedin;
        public bool viewFacebook;
        public bool viewInstagram;

        public bool changeLanguage;

        public int minLeaderboardPosition = _DEFAULT_VALUE;
        public int maxLeaderboardPosition = _DEFAULT_VALUE;

        public bool disableSoundInPause;
        public bool disableMusicInPause;
        public bool disabledVibrationInPause;

        public bool disabledNotifications;

        public int shareGame = _DEFAULT_VALUE;
        public int shareResult = _DEFAULT_VALUE;

        //////////////////////////////////////////

        public void SetMaxWith(SaveStatisticsData saveData)
        {
            totalScore = Mathf.Max(totalScore, saveData.totalScore);
            maxScoreInGame = Mathf.Max(maxScoreInGame, saveData.maxScoreInGame);
            minScoreInGame = Mathf.Max(minScoreInGame, saveData.minScoreInGame);

            totalDistance = Mathf.Max(totalDistance, saveData.totalDistance);
            minDistanceInGame = Mathf.Max(minDistanceInGame, saveData.minDistanceInGame);
            maxDistanceInGame = Mathf.Max(maxDistanceInGame, saveData.maxDistanceInGame);

            totalGems = Mathf.Max(totalGems, saveData.totalGems);
            minGemsInGame = Mathf.Max(minGemsInGame, saveData.minGemsInGame);
            maxGemsInGame = Mathf.Max(maxGemsInGame, saveData.maxGemsInGame);

            totalTurns = Mathf.Max(totalTurns, saveData.totalTurns);
            minTurnsInGame = Mathf.Max(minTurnsInGame, saveData.minTurnsInGame);
            maxTurnsInGame = Mathf.Max(maxTurnsInGame, saveData.maxTurnsInGame);
            maxTurnsOnPlatform = Mathf.Max(maxTurnsOnPlatform, saveData.maxTurnsOnPlatform);

            gamesPlayed = Mathf.Max(gamesPlayed, saveData.gamesPlayed);
            minContinuousDaysPlayed = Mathf.Max(minContinuousDaysPlayed, saveData.minContinuousDaysPlayed);
            maxContinuousDaysPlayed = Mathf.Max(maxContinuousDaysPlayed, saveData.maxContinuousDaysPlayed);
            continuousDaysPlayed = Mathf.Max(continuousDaysPlayed, saveData.continuousDaysPlayed);
            lastDaySession = lastDaySession > saveData.lastDaySession ? lastDaySession : saveData.lastDaySession;

            purchases = Mathf.Max(purchases, saveData.purchases);
            skinsBought = Mathf.Max(skinsBought, saveData.skinsBought);

            rewardedViewed = Mathf.Max(rewardedViewed, saveData.rewardedViewed);
            disabledAds = disabledAds || saveData.disabledAds;

            readPrivacyPolicy = readPrivacyPolicy || saveData.readPrivacyPolicy;
            readTermsofUse = readTermsofUse || saveData.readTermsofUse;
            readPartnersData = readPartnersData || saveData.readPartnersData;

            viewVk = viewVk || saveData.viewVk;
            viewLinkedin = viewLinkedin || saveData.viewLinkedin;
            viewFacebook = viewFacebook || saveData.viewFacebook;
            viewInstagram = viewInstagram || saveData.viewInstagram;

            changeLanguage = changeLanguage || saveData.changeLanguage;

            minLeaderboardPosition = Mathf.Max(minLeaderboardPosition, saveData.minLeaderboardPosition);
            maxLeaderboardPosition = Mathf.Max(maxLeaderboardPosition, saveData.maxLeaderboardPosition);

            disableSoundInPause = disableSoundInPause || saveData.disableSoundInPause;
            disableMusicInPause = disableMusicInPause || saveData.disableMusicInPause;
            disabledVibrationInPause = disabledVibrationInPause || saveData.disabledVibrationInPause;

            disabledNotifications = disabledNotifications || saveData.disabledNotifications;

            shareGame = Mathf.Max(shareGame, saveData.shareGame);
            shareResult = Mathf.Max(shareResult, saveData.shareResult);
        }
    }
}