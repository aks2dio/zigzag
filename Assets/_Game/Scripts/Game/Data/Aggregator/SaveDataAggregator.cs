﻿using System;
using _Game.Scripts.Game.Ads.Save;
using _Game.Scripts.Game.Data.Save;
using _Game.Scripts.Game.IAP.Management.Saving;
using _Game.Scripts.Game.Localization.Save;
using _Game.Scripts.Game.SkinsShop.Save;
using _Game.Scripts.Game.Statistics;

namespace _Game.Scripts.Game.Data.Aggregator
{
    [Serializable]
    public class SaveDataAggregator : ISaveData
    {
        public SaveGameData GameData;
        public SaveSettingsData SettingsData;
        public SaveStatisticsData StatisticsData;
        public SaveRewardedData RewardedData;
        public SavePurchasingData PurchasingData;
        public SaveLocalizationData LocalizationData;
        public SaveSkinsSelectedData SkinsSelectedData;
        public SaveSkinsPurchasesData SkinsPurchasesData;
    }
}