﻿using System;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Data.Save;

namespace _Game.Scripts.Game.Data
{
    public class SettingsData : DataObject<SaveSettingsData>
    {
        #region PRIVATE_VALUES

        private readonly IAnalyticsManager _analytics;

        private bool _soundOn;
        private bool _musicOn;
        private bool _vibration;
        private bool _notifications;

        #endregion // PRIVATE_VALUES

        #region PUBLIC_VALUES

        public bool soundOn
        {
            get => _soundOn;
            set
            {
                if (_soundOn == value)
                    return;

                _soundOn = value;
                InvokeOnChanged();
                onSoundChanged?.Invoke();
                _analytics.SetSound(AnalyticsHelper.GetSetSettingEventData(value));
                Save();
            }
        }

        public bool musicOn
        {
            get => _musicOn;
            set
            {
                if (_musicOn == value)
                    return;

                _musicOn = value;
                InvokeOnChanged();
                onMusicChanged?.Invoke();
                _analytics.SetMusic(AnalyticsHelper.GetSetSettingEventData(value));
                Save();
            }
        }

        public bool vibration
        {
            get => _vibration;
            set
            {
                if (_vibration == value)
                    return;

                _vibration = value;
                InvokeOnChanged();
                onVibrationChanged?.Invoke();
                _analytics.SetVibration(AnalyticsHelper.GetSetSettingEventData(value));
                Save();
            }
        }

        public bool notifications
        {
            get => _notifications;
            set
            {
                if (_notifications == value)
                    return;

                _notifications = value;
                InvokeOnChanged();
                onNotificationsChanged?.Invoke();
                _analytics.SetNotifications(AnalyticsHelper.GetSetSettingEventData(value));
                Save();
            }
        }

        #endregion // PUBLIC_VALUES

        #region EVENTS

        public event Action onSoundChanged;
        public event Action onMusicChanged;
        public event Action onVibrationChanged;
        public event Action onNotificationsChanged;

        #endregion // EVENTS

        protected override string _save_key { get; } = "settings_data";

        ///////////////////////////////////////////////////

        public SettingsData(SaveManager saveManager, IAnalyticsManager analytics) : base(saveManager)
        {
            _analytics = analytics;
        }

        ///////////////////////////////////////////////////

        public override SaveSettingsData CreateSaveData() =>
            new SaveSettingsData()
            {
                soundOn = _soundOn,
                musicOn = _musicOn,
                vibration = _vibration,
                notifications = _notifications
            };

        protected override void OnApply(SaveSettingsData saveData, SaveApplyType applyType)
        {
            _soundOn = ResultDataToApply(saveData.soundOn, _soundOn, applyType);
            _musicOn = ResultDataToApply(saveData.musicOn, _musicOn, applyType);
            _vibration = ResultDataToApply(saveData.vibration, _vibration, applyType);
            _notifications = ResultDataToApply(saveData.notifications, _notifications, applyType);

            onSoundChanged?.Invoke();
            onMusicChanged?.Invoke();
            onVibrationChanged?.Invoke();
            onNotificationsChanged?.Invoke();
        }
    }
}