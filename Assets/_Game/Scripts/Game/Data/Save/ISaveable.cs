﻿
namespace _Game.Scripts.Game.Data.Save
{
    public interface ISaveable
    {
        void Save();
    }
}
