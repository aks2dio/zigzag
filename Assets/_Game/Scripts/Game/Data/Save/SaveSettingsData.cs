﻿using System;

namespace _Game.Scripts.Game.Data.Save
{
    [Serializable]
    public class SaveSettingsData : ISaveData
    {
        public bool soundOn = true;
        public bool musicOn = true;
        public bool vibration = true;
        public bool notifications = true;
    }
}
