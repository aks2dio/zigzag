﻿using _Game.Scripts.UI.Elements;
using UnityEngine;

namespace _Game.Scripts.Game.Data.UI
{
    [RequireComponent(typeof(Switch))]
    internal class NotificationsSwitch : SettingsDataSwitch
    {
        protected override bool _data => _settingsData.notifications;

        protected override void OnSwitch(bool result)
        {
            _settingsData.notifications = result;

            if (result == false)
                _statistics.SetNotificationsDisabled();
        }
    }
}
