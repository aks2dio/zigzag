﻿
namespace _Game.Scripts.Game.Data.UI
{
    class NewRecordHidingObject : GameDataAutoComponentsEnabler<bool>
    {
        protected override bool _data => _gameData.newRecord;
        protected override bool _isEnabled => _data;
    }
}
