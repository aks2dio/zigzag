﻿using _Game.Scripts.Game.Statistics;
using _Game.Scripts.UI.Elements;
using Core.Components.UI;
using Core.Components.UI.Screens;
using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Data.UI
{
    [RequireComponent(typeof(Switch))]
    abstract class SettingsDataSwitch : MyMonoBehaviour
    {
        [Inject] protected StatisticsData _statistics { get; }
        [Inject] protected SettingsData _settingsData { get; }
        [Inject] protected IUIManager _ui { get; }

        private Switch _switch;

        protected abstract bool _data { get; }

        ///////////////////////////////////////////////

        private void OnEnable()
        {
            OnAttaching();

            _switch.onSwitch.AddListener(OnSwitch);
            _switch.SetEnabled(_data);
        }

        private void OnDisable()
        {
            _switch.onSwitch.RemoveListener(OnSwitch);
        }

        protected override void OnAttaching()
            => Attach(ref _switch);

        protected bool IsDisabledDuringTheGame(bool result) =>
            !result && _ui.ContainsInQueue(ScreenType.Game);

        protected abstract void OnSwitch(bool result);
    }
}