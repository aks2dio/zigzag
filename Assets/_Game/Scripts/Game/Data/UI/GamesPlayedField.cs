﻿using Core.Components;
using UnityEngine;

namespace _Game.Scripts.Game.Data.UI
{
    [RequireComponent(typeof(CompositeText))]
    class GamesPlayedField : GameDataField<int>
    {
        protected override int _data => _gameData.gamesPlayed;

        protected override void UpdateText(int value) =>
            _textComponent.SetPart(value, 1);
    }
}