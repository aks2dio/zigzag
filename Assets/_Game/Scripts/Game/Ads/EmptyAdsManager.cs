﻿namespace _Game.Scripts.Game.Ads
{
    public class EmptyAdsManager : IAdsManager
    {
        public IAdsPlayer banner { get; }
        public IAdsPlayer video { get; }
        public IAdsPlayer rewarded { get; }
        
        public bool initialized { get; }

        ////////////////////////////////////
        
        public EmptyAdsManager()
        {
            banner = new EmptyAdsPlayer();
            video = new EmptyAdsPlayer();
            rewarded = new EmptyAdsPlayer();
            initialized = true;
        }
    }
}