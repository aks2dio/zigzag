﻿using System.Collections.Generic;
using _Game.Scripts.Game.Ads.AdMob.Players;
using _Game.Scripts.Game.Ads.Settings;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.IAP.Management.Saving;
using _Game.Scripts.Game.Statistics;
using GoogleMobileAds.Api;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Ads.AdMob
{
    public class AdMobManager : LoggedMonoBehaviour, IAdsManager
    {
        [Inject] private IAnalyticsManager _Analytics { get; }
        [Inject] private StatisticsData _Statistics { get; }
        [Inject] private PurchasingData _PurchasingData { get; }
        [Inject] private AdvertisementSettings _Settings { get; }

        public IAdsPlayer banner { get; private set; }
        public IAdsPlayer video { get; private set; }
        public IAdsPlayer rewarded { get; private set; }

        public bool initialized { get; private set; }
        
        ////////////////////////////////////////////////////

        private void Start()
        {
            MobileAds.Initialize(OnInitialized);
        }
        
        private void OnInitialized(InitializationStatus initStatus)
        {
            Log(initStatus.ToString());

            var requestBuilder = CreateRequestBuilder(_Settings);
            banner = CreateBannerPlayer(_Settings.bannerSettings, requestBuilder);
            video = CreateVideoPlayer(_Settings.videoSettings, requestBuilder);
            rewarded = CreateRewardedPlayer(_Settings.rewardedSettings, requestBuilder);

            if (_PurchasingData.noAds)
                DisableAds();
            else
                _PurchasingData.onNoAdsActivated += DisableAds;

            initialized = true;
        }

        private void Update()
        {
            if (!initialized)
                return;
            
            var dt = Time.unscaledTime;
            
            video.Update(dt);
            banner.Update(dt);
            rewarded.Update(dt);
        }

        ////////////////////////////////////////////////////

        private AdRequest.Builder CreateRequestBuilder(AdvertisementSettings settings)
        {
            // создадим список девайсов
            var testDevices = new List<string>() { AdRequest.TestDeviceSimulator };
            testDevices.AddRange(settings.testDeviceIds);
            
            // настроим среду
            var requestConfiguration = new RequestConfiguration.Builder()
                .SetTestDeviceIds(testDevices)
                .build();

            MobileAds.SetRequestConfiguration(requestConfiguration);
            
            // создадим билдера
            var requestBuilder = new AdRequest.Builder();
            foreach (var testDevice in testDevices)
                requestBuilder.AddTestDevice(testDevice);
            
            return requestBuilder;
        }
        
        private IAdsPlayer CreateBannerPlayer(AdsBannerSettings settings, AdRequest.Builder requestBuilder)
        {
            var player = settings.enabled ?
                new BannerAdsPlayer(settings, requestBuilder) :
                new EmptyAdsPlayer() as IAdsPlayer;

            player.onStart += _Analytics.ShowBanner;
            return player;
        }

        private IAdsPlayer CreateVideoPlayer(AdsVideoSettings settings, AdRequest.Builder requestBuilder)
        {
            var player = settings.enabled ?
                new VideoAdsPlayer(settings, requestBuilder) :
                new EmptyAdsPlayer() as IAdsPlayer;

            player.onStart += _Analytics.ShowAds;
            return player;
        }

        private IAdsPlayer CreateRewardedPlayer(AdsRewardedSettings settings, AdRequest.Builder requestBuilder)
        {
            var player = settings.enabled ?
                new RewardedVideoAdsPlayer(settings, requestBuilder) :
                new EmptyAdsPlayer() as IAdsPlayer;

            player.onStart += _Analytics.ShowRewardAds;
            player.onFinish += _Statistics.AddRewardedViewed;
            return player;
        }

        ////////////////////////////////////////////////////

        private void DisableAds()
        {
            banner.Hide();
            video.Hide();

            banner = new EmptyAdsPlayer();
            video = new EmptyAdsPlayer();
        }
    }
}