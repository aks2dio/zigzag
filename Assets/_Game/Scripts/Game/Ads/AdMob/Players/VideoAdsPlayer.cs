﻿using _Game.Scripts.Game.Ads.Settings;
using GoogleMobileAds.Api;

namespace _Game.Scripts.Game.Ads.AdMob.Players
{
    public class VideoAdsPlayer : AdMobAdsPlayer
    {
        private InterstitialAd _interstitial;
        
        ////////////////////////////////////
        
        public VideoAdsPlayer(AdsVideoSettings settings, AdRequest.Builder requestBuilder) : base(requestBuilder)
        {
            _interstitial = new InterstitialAd(settings.placementId);
            
            _interstitial.OnAdLoaded += HandleOnLoad;
            _interstitial.OnAdOpening += HandleOnStart;
            _interstitial.OnAdClosed += HandleOnClose;
            _interstitial.OnAdFailedToLoad += HandleOnError;
            _interstitial.OnAdLeavingApplication += HandleOnLeavingApp;
            
            LoadAd();
        }

        ////////////////////////////////////

        protected override void StartShow() => 
            _interstitial.Show();

        protected override void StopShow() 
        { }
        
        protected sealed override void LoadAd()
        {
            Log("Load ad");
            _interstitial.LoadAd(CreateRequest());
        }
    }
}