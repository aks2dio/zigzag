﻿using System;
using Core;
using GoogleMobileAds.Api;

namespace _Game.Scripts.Game.Ads.AdMob.Players
{
    public abstract class AdMobAdsPlayer : LoggedClass, IAdsPlayer
    {
        public event Action onStart;
        public event Action onFinish;
        public event Action onStop;
        public event Action onError;
        public event Action onAdsReady;
        public event Action onAdsReadyChange;

        public bool isReady { get; private set; }
        public bool isShowing { get; private set; }

        protected bool _isFinished;
        
        private readonly AdRequest.Builder _requestBuilder;
        private bool _needLaunch;

        private bool _onLoadTrigger;
        private bool _onStartTrigger;
        private bool _onFinishTrigger;
        private bool _onCloseTrigger;
        private bool _onErrorTrigger;
        
        private Action _onClose;
        private Action _onFinished;
        private Action _onError;
        
        ////////////////////////////////////

        protected AdMobAdsPlayer(AdRequest.Builder requestBuilder) =>
            _requestBuilder = requestBuilder;

        ////////////////////////////////////

        // т.к. события от AdMob проихсодят не в главном потоке
        public void Update(float dt)
        {
            TryTriggerCloseEvent();
            TryTriggerFinishEvent();
            TryTriggerLoadEvent();
            TryTriggerStartEvent();
            TryTriggerErrorEvent();
        }

        ////////////////////////////////////

        #region TRIGGER_LAUNCHING

        private void TryTriggerCloseEvent()
        {
            if (!_onCloseTrigger)
                return;

            Log("Close from Update");
            _onCloseTrigger = false;
            _onClose?.Invoke();
            
            SetIsReady(false);
            LoadAd();
        }

        private void TryTriggerFinishEvent()
        {
            if (!_onFinishTrigger)
                return;
            
            Log("Finish from Update");
            _onFinishTrigger = false;
            
            _onFinished?.Invoke();
            onFinish?.Invoke();
        }

        private void TryTriggerLoadEvent()
        {
            if (!_onLoadTrigger)
                return;
            
            Log("Load from Update");
            _onLoadTrigger = false;
            
            SetIsReady(true);
            TryShow();
        }

        private void TryTriggerStartEvent()
        {
            if (!_onStartTrigger)
                return;
            
            Log("Start from Update");
            _onStartTrigger = false;
            
            onStart?.Invoke();
        }

        private void TryTriggerErrorEvent()
        {
            if (!_onErrorTrigger)
                return;
            
            Log("Error from Update");
            _onErrorTrigger = false;
            
            onError?.Invoke();
            _onError?.Invoke();
            
            SetIsReady(false);
            LoadAd();
        }

        #endregion // TRIGGER_LAUNCHING

        ////////////////////////////////////

        public void Show(Action onClose = null, Action onFinished = null, Action onError = null)
        {
            if (isShowing || _needLaunch)
            {
                Log("Already showing or need launch");
                return;
            }

            _onClose = onClose;
            _onFinished = onFinished;
            _onError = onError;
            _needLaunch = true;

            TryShow();
        }

        public void Hide()
        {
            _needLaunch = false;

            if (isShowing == false)
            {
                Log("Can't hide - it's not showing");
                return;
            }

            _onClose = null;
            _onFinished = null;
            _onError = null;
            isShowing = false;

            Log("Hided");
            
            StopShow();
            onStop?.Invoke();
        }
        
        ////////////////////////////////////

        private void TryShow()
        {
            if (!isShowing && _needLaunch && isReady) 
            {
                Log("Try start show");
                StartShow();
            }
        }
        
        private void SetIsReady(bool status)
        {
            if (isReady == status)
                return;

            isReady = status;
            Log($"{nameof(isReady)} = {status}");
            
            onAdsReadyChange?.Invoke();
            
            if (isReady)
                onAdsReady?.Invoke();
        }

        ////////////////////////////////////

        protected abstract void StartShow();
        protected abstract void StopShow();
        protected abstract void LoadAd();

        protected AdRequest CreateRequest() => 
            _requestBuilder.Build();
        
        ////////////////////////////////////

        #region HANDLING

        protected void HandleOnLoad(object sender, EventArgs args)
        {
            Log("HandleOnLoad");
            _onLoadTrigger = true;            
        }

        protected void HandleOnStart(object sender, EventArgs args)
        {
            Log("HandleOnStart");
            
            isShowing = true;
            _needLaunch = false;
            _isFinished = false;
            
            _onStartTrigger = true;
        }

        protected void HandleOnFinish(object sender, EventArgs args)
        {
            Log("HandleOnFinish");
            
            _needLaunch = false;

            if (isShowing)
            {
                isShowing = false;
                _isFinished = true;
            }
            else
            {
                Log("Trigger Finish from Finish");
                _onFinishTrigger = true;
            }
        }

        protected void HandleOnClose(object sender, EventArgs args)
        {
            Log("HandleOnClose");
            
            isShowing = false;
            _needLaunch = false;
            
            if (_isFinished)
            {
                Log("Trigger Finish from Close");
                _onFinishTrigger = true;
            }
            
            _isFinished = false;
            _onCloseTrigger = true;
        }

        protected void HandleOnLeavingApp(object sender, EventArgs args) =>
            Log("HandleOnLeavingApp");

        protected void HandleOnError(object sender, AdFailedToLoadEventArgs args) =>
            HandleOnError(args.Message);

        protected void HandleOnError(object sender, AdErrorEventArgs args) =>
            HandleOnError(args.Message);

        private void HandleOnError(string message)
        {
            Error($"Error: {message}");
            
            isShowing = false;
            _needLaunch = false;
            _isFinished = false;

            _onErrorTrigger = true;
        }

        #endregion // HANDLING
    }
}