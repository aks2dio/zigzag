﻿using System.Collections;
using _Game.Scripts.Game.Ads.Save;
using _Game.Scripts.Game.Ads.Settings;
using _Game.Scripts.Game.Ads.UI;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.UI.Reward;
using Core.Components.UI;
using Core.Components.UI.Screens;
using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Ads.Showers
{
    class AdsRewardedByButtonShower : MyMonoBehaviour
    {
        [Inject] private IAnalyticsManager _analytics { get; }
        [Inject] private RewardedCountingData _data { get; }
        [Inject] private IUIManager _uiManager { get; }
        
        [Inject] private AdsRewardedByButtonSettings _Settings { get; } 
        [Inject] private IAdsManager _AdsManager { get; }

        
        [Header("Attachments")]
        [SerializeField] private AdsRewardedButton _Button = null;

        [Header("Events")]
        public IntUnityEvent OnRewarded = new IntUnityEvent();

        private int _rewardCache;

        private bool _canSeeAdsNow =>
            _rewardedPlayer != null && !_rewardedPlayer.isShowing && _rewardedPlayer.isReady &&
            _Settings != null && _Settings.CanSeeAdsNow(_data.viewsCounter) &&
            !_data.isCounterFull;

        private IAdsPlayer _rewardedPlayer => _AdsManager?.rewarded;

        ///////////////////////////////////////////////////

        private IEnumerator Start()
        {
            yield return new WaitUntil(() => _data != null && _analytics != null && _rewardedPlayer != null);
            
            _data.onCounterChanged += UpdateVisibility;
            _data.onCounterReset += OnCounterReset;
            _rewardedPlayer.onAdsReadyChange += UpdateVisibility;
            _Button.onClick += Launch;
            
            if (_data.needSetViews)
                OnCounterReset();
            
            UpdateVisibility();
        }

        private void OnDestroy()
        {
            _data.onCounterChanged -= UpdateVisibility;
            _data.onCounterReset -= OnCounterReset;
            _rewardedPlayer.onAdsReadyChange -= UpdateVisibility;

            _Button.onClick -= Launch;
        }

        ///////////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(_Button);
        }

        ///////////////////////////////////////////////////

        public void UpdateVisibility()
        {
            _Button.SetActive(_canSeeAdsNow);
            _Button.UpdateData(_rewardCache);
        }

        ///////////////////////////////////////////////////

        private void Launch()
        {
            if (_Button.isActive == false)
                return;

            _rewardCache = _data.useMaxReward ? _Settings.maxReward : _Settings.randomReward;
            _rewardedPlayer.Show(onFinished: OnAdsFinished);
        }

        private void OnAdsFinished()
        {
            var screen = _uiManager.Open(ScreenType.Reward);
            var view = screen.gameObject.GetComponent<RewardViewBehaviour>();
            view.Init(_rewardCache, () => OnRewarded?.Invoke(_rewardCache));
            
            _data.IncreaseCounter();
            _data.useMaxReward = _data.useMaxReward && _data.viewsCounter < _Settings.TimesToGiveMaxReward;
            _analytics.UseFreeGems(AnalyticsHelper.GetFreeGemsEventData(_rewardCache));
        }

        private void OnCounterReset()
            => _data.SetViewsForToday(_Settings.randomViewsForToday);
    }
}
