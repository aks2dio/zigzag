﻿using System.Collections;
using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Data;
using _Game.Scripts.UI.Reward;
using Core.Components.UI;
using Core.Components.UI.Screens;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Game.Ads.Showers
{
    class AdsRewardedDoublerShower : MyMonoBehaviour
    {
        [Inject] private GameData _gameData { get; }
        [Inject] private IUIManager _uiManager { get; }
        [Inject] private IAnalyticsManager _analytics { get; }
        
        [Inject] private IAdsManager _AdsManager { get; }
        
        [Header("Attachments")]
        [SerializeField] private Button _Button = null;

        [Header("Events")]
        public IntUnityEvent OnRewarded = new IntUnityEvent();

        private int _rewardCache;

        private bool _canSeeAdsNow => _rewardedPlayer != null && _rewardedPlayer.isReady;

        private bool _isButtonActive => _Button.gameObject.activeSelf;
        private IAdsPlayer _rewardedPlayer => _AdsManager?.rewarded;

        ///////////////////////////////////////////////////

        protected override void OnChecking()
        {
            CheckField(_Button);
        }
        
        private IEnumerator Start()
        {
            yield return new WaitUntil(() => _gameData != null && _analytics != null && _rewardedPlayer != null);

            _Button.onClick.AddListener(Launch);
            _rewardedPlayer.onAdsReadyChange += UpdateVisibility;

            UpdateVisibility();
        }

        private void OnDestroy()
        {
            _Button.onClick.RemoveListener(Launch);
            _rewardedPlayer.onAdsReadyChange -= UpdateVisibility;
        }

        ///////////////////////////////////////////////////

        public void UpdateVisibility() =>
            _Button.gameObject.SetActive(_canSeeAdsNow);

        ///////////////////////////////////////////////////

        private void Launch()
        {
            if (_isButtonActive == false)
                return;

            _rewardCache = _gameData?.currentGems ?? 0;
            _rewardedPlayer?.Show(onFinished: OnAdsFinished);
        }

        private void OnAdsFinished()
        {
            var screen = _uiManager.Open(ScreenType.Reward);
            var view = screen.gameObject.GetComponent<RewardViewBehaviour>();
            view.Init(_rewardCache * 2, () => OnRewarded?.Invoke(_rewardCache));
            
            _analytics.UseDoubleReward(AnalyticsHelper.GetDoubleRewardEventData(_rewardCache));
        }
    }
}
