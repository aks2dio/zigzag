﻿using _Game.Scripts.Game.Ads.Settings;
using UnityEngine.Advertisements;

namespace _Game.Scripts.Game.Ads.UnityAds.Players
{
    class BannerAdsPlayer : UnityAdsPlayer
    {
        private readonly AdsBannerSettings _settings;

        ///////////////////////////////////////////////////////////

        public BannerAdsPlayer(AdsBannerSettings settings) : base()
        {
            _settings = settings;
            SettingBanner(_settings);
        }

        ///////////////////////////////////////////////////////////

        protected override bool IsIdMine(string placementId) =>
            _settings.placementId == placementId;

        protected override void StartShow() =>
            Advertisement.Banner.Show(_settings.placementId);

        protected override void StopShow() =>
            Advertisement.Banner.Hide();

        //////////////////////////////////////////////////

        // у баннеры не вызывается OnUnityAdsStart
        protected override void OnAdsReady() => isShowing = true;

        //////////////////////////////////////////////////

        private void SettingBanner(AdsBannerSettings settings)
        {
            Advertisement.Banner.SetPosition(settings.bannerPosition);
        }
    }
}
