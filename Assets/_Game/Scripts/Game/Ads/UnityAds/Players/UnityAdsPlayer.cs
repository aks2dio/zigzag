﻿using System;
using Core;
using UnityEngine.Advertisements;

namespace _Game.Scripts.Game.Ads.UnityAds.Players
{
    abstract class UnityAdsPlayer : LoggedClass, IUnityAdsListener, IAdsPlayer
    {
        private bool _needLaunch;
        private Action _onClose;
        private Action _onFinished;
        private Action _onError;

        public bool isReady { get; private set; }
        public bool isShowing { get; protected set; }
        
        public event Action onStart;
        public event Action onStop;
        public event Action onError;
        public event Action onFinish;
        public event Action onAdsReady;
        public event Action onAdsReadyChange;

        //////////////////////////////////////////////////

        public UnityAdsPlayer()
        {
            Advertisement.AddListener(this);
        }

        ~UnityAdsPlayer()
        {
            Advertisement.RemoveListener(this);
        }

        //////////////////////////////////////////////////

        public void Show(Action onClose = null, Action onFinished = null, Action onError = null)
        {
            if (isShowing)
                return;

            _onClose = onClose;
            _onFinished = onFinished;
            _onError = onError;
            _needLaunch = true;

            TryShow();
        }

        public void Hide()
        {
            _needLaunch = false;

            if (isShowing == false)
                return;

            isShowing = false;

            StopShow();
            onStop?.Invoke();
        }
        
        public void Update(float dt)
        { }

        //////////////////////////////////////////////////

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            if (IsIdMine(placementId) == false)
                return;

            isShowing = false;
            _needLaunch = false;

            switch (showResult) {
                case ShowResult.Finished: 
                    _onFinished?.Invoke(); 
                    onFinish?.Invoke();
                    Log("Finish show");
                    break;

                case ShowResult.Skipped:
                    _onClose?.Invoke(); 
                    Log("Skipped");
                    break;

                case ShowResult.Failed:
                    _onError?.Invoke();
                    Error("Error");
                    break;
            }
        }

        public void OnUnityAdsDidStart(string placementId)
        {
            if (isReady == false)
                return;
            
            if (IsIdMine(placementId) == false)
                return;

            isShowing = true;
            _needLaunch = false;

            OnAdsStart();
            Log("Start: " + placementId);
        }

        public void OnUnityAdsDidError(string message)
        {
            isShowing = false;
            _needLaunch = false;

            OnAdsError();
            _onError?.Invoke();
            onError?.Invoke();
            Error("Error: " + message);
        }

        public void OnUnityAdsReady(string placementId)
        {
            if (isReady)
                return;
            
            if (IsIdMine(placementId) == false)
                return;

            SetIsReady(true);
            TryShow();

            OnAdsReady();
            onAdsReady?.Invoke();
            Log("Ready: " + placementId);
        }

        //////////////////////////////////////////////////

        protected abstract bool IsIdMine(string placementId);
        protected abstract void StartShow();
        protected abstract void StopShow();
        protected virtual void OnAdsReady() { }
        protected virtual void OnAdsError() { }
        protected virtual void OnAdsStart() { }

        //////////////////////////////////////////////////

        private void TryShow()
        {
            if (!isShowing && _needLaunch && isReady) {
                StartShow();
                SetIsReady(false);
                Log("Start show");
                onStart?.Invoke();
            }
        }

        private void SetIsReady(bool status)
        {
            if (isReady == status)
                return;

            isReady = status;
            onAdsReadyChange?.Invoke();
        }
    }
}
