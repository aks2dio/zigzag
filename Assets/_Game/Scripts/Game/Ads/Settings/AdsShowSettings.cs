﻿using UnityEngine;

namespace _Game.Scripts.Game.Ads.Settings
{
    [CreateAssetMenu(fileName = nameof(AdsShowSettings), menuName = DefinitionsHelper.Parameters + nameof(AdsShowSettings))]
    class AdsShowSettings : ScriptableObject
    {
        [SerializeField] private AnimationCurve _LaunchAfterGames = new AnimationCurve();

        public AnimationCurve launchAfterGame => _LaunchAfterGames;
    }
}
