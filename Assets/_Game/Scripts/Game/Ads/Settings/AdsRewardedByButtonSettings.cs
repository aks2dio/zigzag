﻿using Core.Structs.Weights;
using UnityEngine;

namespace _Game.Scripts.Game.Ads.Settings
{
    [CreateAssetMenu(fileName = nameof(AdsRewardedByButtonSettings), menuName = DefinitionsHelper.Parameters + nameof(AdsRewardedByButtonSettings))]
    class AdsRewardedByButtonSettings : ScriptableObject
    {
        [SerializeField] private WeightedValueCollectionInt _Rewards;
        [SerializeField] private int _MinViewsForToday = 0;
        [SerializeField] private int _MaxViewsForToday = 0;
        [SerializeField] private int _TimesToGiveMaxReward = 0;
        [SerializeField] private AnimationCurve _ShowingProbabilityByViews = new AnimationCurve();

        public int TimesToGiveMaxReward => _TimesToGiveMaxReward;
        public int randomReward => _Rewards.randomValue;
        public int maxReward => _Rewards.maxValue;
        public int minReward => _Rewards.minValue;

        public int randomViewsForToday =>
            Random.Range(
                Mathf.Min(_MinViewsForToday, _MaxViewsForToday),
                Mathf.Max(_MinViewsForToday, _MaxViewsForToday) + 1
                );

        public bool CanSeeAdsNow(int views) =>
            views < _MaxViewsForToday &&
            Random.Range(0f, 1f) <= _ShowingProbabilityByViews.Evaluate(views);

        //////////////////////////////////////////////////////////////////////////

        private void OnValidate()
        {
            if (_MaxViewsForToday < _MinViewsForToday)
                _MaxViewsForToday = _MinViewsForToday;

            if (_TimesToGiveMaxReward >= _MaxViewsForToday)
                _TimesToGiveMaxReward = _MaxViewsForToday - 1;
        }
    }
}
