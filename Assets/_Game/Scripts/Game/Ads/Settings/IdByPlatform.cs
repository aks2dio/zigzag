﻿using System;
using UnityEngine;

namespace _Game.Scripts.Game.Ads.Settings
{
    [Serializable]
    public struct IdByPlatform
    {
        [SerializeField] private RuntimePlatform _Platform;
        [SerializeField] private string _Id;

        public RuntimePlatform platfrom => _Platform;
        public string id => _Id;

        public IdByPlatform(RuntimePlatform platform, string id)
        {
            _Platform = platform;
            _Id = id;
        }
    }
}
