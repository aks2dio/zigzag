﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

namespace _Game.Scripts.Game.Ads.Settings
{
    [Serializable]
    public struct AdsBannerSettings
    {
        [SerializeField] private bool _Enabled;
        [SerializeField] private string _PlacementId;
        [SerializeField] private BannerPosition _BannerPosition;

        public bool enabled => _Enabled;
        public string placementId => _PlacementId;
        public BannerPosition bannerPosition => _BannerPosition;


        public AdsBannerSettings(bool enabled, string id, BannerPosition position)
        {
            _Enabled = enabled;
            _PlacementId = id;
            _BannerPosition = position;
        }
    }
}
