﻿using System;
using Core.Interfaces;

namespace _Game.Scripts.Game.Ads
{
    public interface IAdsPlayer : IUpdatable
    {
        event Action onStart;
        event Action onFinish;
        event Action onError;
        event Action onStop;
        event Action onAdsReady;
        event Action onAdsReadyChange;

        bool isReady { get; }
        bool isShowing { get; }

        void Show(Action onClose = null, Action onFinished = null, Action onError = null);
        void Hide();
    }
}