﻿using Core.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Game.Scripts.Game.Ads.UI
{
    [RequireComponent(typeof(Button))]
    class AdsRewardedButton : MyMonoBehaviour
    {
        private Button _button => GetComponent<Button>();


        public bool isActive => gameObject.activeSelf;

        public event UnityAction onClick
        {
            add => _button.onClick.AddListener(value);
            remove => _button.onClick.RemoveListener(value);
        }

        /////////////////////////////////////////////////////////

        public void UpdateData(int rewardCount) { }

        public void SetActive(bool status) =>
            gameObject.SetActive(status);
    }
}
