﻿namespace _Game.Scripts.Game.Ads.Observing
{
    class VideoSubscriber : PlayerSubscriber<VideoListener>
    {
        protected override IAdsPlayer _player => _AdsManager.video;
    }
}
