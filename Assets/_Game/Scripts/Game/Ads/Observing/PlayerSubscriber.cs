﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Ads.Observing
{
    abstract class PlayerSubscriber<T> : LoggedMonoBehaviour where T : PlayerListener
    {
        [Inject] protected IAdsManager _AdsManager { get; }
        
        [SerializeField] private bool _GlobalSearch = false;
        [SerializeField] private Transform _SearchInChildren = null;

        protected abstract IAdsPlayer _player { get; }

        /////////////////////////////////////////////////////////

        private IEnumerator Start()
        {
            if (_AdsManager == null)
                yield break;

            yield return new WaitUntil(() => _AdsManager.initialized);

            if (_player == null)
                yield break;

            foreach (var listener in GetListeners())
            {
                Subscribe(_player, listener);
                listener.onDestroy.AddListener(() => Unsubscribe(_player, listener));

                if (_player.isShowing)
                    listener.NotifyShowing();
            }
        }

        /////////////////////////////////////////////////////////

        private T[] GetListeners() =>
            _GlobalSearch ?
                FindObjectsOfType<T>() :
                _SearchInChildren?.GetComponentsInChildren<T>(true) ?? new T[0];

        /////////////////////////////////////////////////////////

        protected virtual void Subscribe(IAdsPlayer adsPlayer, T listener)
        {
            adsPlayer.onStop += listener.NotifyHided;
            adsPlayer.onError += listener.NotifyHided;
            adsPlayer.onFinish += listener.NotifyHided;
            adsPlayer.onStart += listener.NotifyShowing;
            
            Log("Subscribe");
        }

        protected virtual void Unsubscribe(IAdsPlayer adsPlayer, T listener)
        {
            adsPlayer.onStop -= listener.NotifyHided;
            adsPlayer.onError -= listener.NotifyHided;
            adsPlayer.onFinish -= listener.NotifyHided;
            adsPlayer.onStart -= listener.NotifyShowing;
            
            Log("Unsubscribe");
        }
    }
}