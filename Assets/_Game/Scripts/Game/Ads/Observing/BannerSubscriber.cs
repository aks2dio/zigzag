﻿namespace _Game.Scripts.Game.Ads.Observing
{
    class BannerSubscriber : PlayerSubscriber<BannerListener>
    {
        protected override IAdsPlayer _player => _AdsManager?.banner;
        
        protected override void Subscribe(IAdsPlayer adsPlayer, BannerListener listener)
        {
            adsPlayer.onStop += listener.NotifyHided;
            adsPlayer.onError += listener.NotifyHided;
            adsPlayer.onFinish += listener.NotifyHided;
            adsPlayer.onAdsReady += listener.NotifyShowing;
        }
        
        protected override void Unsubscribe(IAdsPlayer adsPlayer, BannerListener listener)
        {
            adsPlayer.onStop -= listener.NotifyHided;
            adsPlayer.onError -= listener.NotifyHided;
            adsPlayer.onFinish -= listener.NotifyHided;
            adsPlayer.onAdsReady -= listener.NotifyShowing;
        }
    }
}
