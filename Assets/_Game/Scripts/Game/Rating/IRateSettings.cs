﻿using System.Collections.Generic;

namespace _Game.Scripts.Game.Rating
{
    interface IRateSettings
    {
        IEnumerable<UrlByPlatform> urlByPlatforms { get; }
        string currentUrl { get; }
    }
}
