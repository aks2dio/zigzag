﻿using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Rating
{
    class RateButton : MonoBehaviour
    {
        [Inject] private IRateSettings _Settings { get; }

        public void Click()
        {
            UnityEngine.Debug.Log($"[{GetType().Name}] Clicked");
            Application.OpenURL(_Settings.currentUrl);
        }
    }
}
