﻿using System.Collections.Generic;
using Core.Utilities;
using UnityEngine;

namespace _Game.Scripts.Game.Rating
{
    [CreateAssetMenu(fileName = nameof(RateSettings), menuName = DefinitionsHelper.Parameters + nameof(RateSettings))]
    class RateSettings : ScriptableObject, IRateSettings
    {
        [SerializeField] private UrlByPlatform[] _Urls = new UrlByPlatform[0];

        public IEnumerable<UrlByPlatform> urlByPlatforms => _Urls;

        public string currentUrl
        {
            get
            {
                var currentPlatform = PlatformsUtility.GetCurrentPlatfrom();
                
                foreach (var url in _Urls)
                    if (url.Platform == currentPlatform)
                        return url.Url;
                
                return "";
            }
        }
    }
}
