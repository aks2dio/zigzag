﻿using System.Collections.Generic;
using _Game.Scripts.Game.Social;
using UnityEngine;

namespace _Game.Scripts.Game.Analytics
{
    #region ENUMS

    public enum RestartGameType
    {
        PauseMenu,
        GameOver
    }

    #endregion // ENUMS

    #region REVENUE

    public interface IRevenueData { }

    public interface IUnityRevenueData : IRevenueData
    {
        UnityAnalyticsRevenue ToUnityRevenueData();
    }

    public interface IUnityReceiptData
    {
        UnityAnalyticsReceipt ToUnityReceiptData();
    }

    public interface IYandexRevenueData : IRevenueData
    {
        YandexAppMetricaRevenue ToYandexRevenueData();
    }

    public interface IYandexReceiptData
    {
        YandexAppMetricaReceipt ToYandexReceiptData();
    }

    public struct ReceiptEventData : IYandexReceiptData, IUnityReceiptData
    {
        public readonly string data;
        public readonly string signature;
        public readonly string transactionId;

        public ReceiptEventData(string data, string signature, string transactionId)
        {
            this.data = data;
            this.signature = signature;
            this.transactionId = transactionId;
        }

        public YandexAppMetricaReceipt ToYandexReceiptData() =>
            new YandexAppMetricaReceipt()
            {
                Data = data,
                Signature = signature,
                TransactionID = transactionId
            };

        public UnityAnalyticsReceipt ToUnityReceiptData() =>
            new UnityAnalyticsReceipt()
            {
                Data = data,
                Signature = signature,
                TransactionID = transactionId
            };
    }

    public struct PurchaseEventData : IYandexRevenueData, IUnityRevenueData
    {
        public readonly string purchaseId;
        public readonly decimal price;
        public readonly string currency;
        public readonly int quantity;
        public readonly string payload;
        public readonly ReceiptEventData receipt;

        public PurchaseEventData(string purchaseId, decimal price, string currency,
            int quantity, string payload, ReceiptEventData receipt)
        {
            this.purchaseId = purchaseId;
            this.price = price;
            this.currency = currency;
            this.quantity = quantity;
            this.payload = payload;
            this.receipt = receipt;
        }

        public YandexAppMetricaRevenue ToYandexRevenueData() =>
            new YandexAppMetricaRevenue(price, currency)
            {
                ProductID = purchaseId,
                Quantity = quantity,
                Payload = payload,
                Receipt = receipt.ToYandexReceiptData()
            };

        public UnityAnalyticsRevenue ToUnityRevenueData() =>
            new UnityAnalyticsRevenue()
            {
                ProductID = purchaseId,
                Currency = currency,
                Quantity = quantity,
                Payload = payload,
                Price = (float)price,
                Receipt = receipt.ToUnityReceiptData()
            };
    }

    public struct UnityAnalyticsRevenue
    {
        public string ProductID;
        public int Quantity;
        public float Price;
        public string Currency;
        public string Payload;
        public UnityAnalyticsReceipt Receipt;
    }

    public struct UnityAnalyticsReceipt
    {
        public string Data;
        public string Signature;
        public string TransactionID;
    }

    #endregion // REVENUE

    #region EVENT_DATA

    public interface IEventData { }

    public interface IStringObjectEventData : IEventData
    {
        Dictionary<string, object> ToStringObjectData();
    }

    public struct AttemptEventData : IStringObjectEventData
    {
        public readonly int score;
        public readonly int distance;
        public readonly int gems;
        public readonly int turns;

        public AttemptEventData(int score, int distance, int gems, int turns)
        {
            this.score = score;
            this.distance = distance;
            this.gems = gems;
            this.turns = turns;
        }

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(distance), distance },
                { nameof(score), score },
                { nameof(gems), gems },
                { nameof(turns), turns },
            };
    }

    public struct GameOverEventData : IStringObjectEventData
    {
        public readonly int gamesPlayed;
        public readonly AttemptEventData attemptData;

        public GameOverEventData(int gamesPlayed, AttemptEventData attemptData)
        {
            this.gamesPlayed = gamesPlayed;
            this.attemptData = attemptData;
        }

        public Dictionary<string, object> ToStringObjectData()
        {
            var result = attemptData.ToStringObjectData();
            result[nameof(gamesPlayed)] = gamesPlayed;

            return result;
        }
    }

    public struct DoubleRewardEventData : IStringObjectEventData
    {
        public readonly int initialGems;
        public readonly int targetGems;

        public DoubleRewardEventData(int initialGems, int targetGems)
        {
            this.initialGems = initialGems;
            this.targetGems = targetGems;
        }

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(initialGems), initialGems },
                { nameof(targetGems), targetGems }
            };
    }

    public struct RestartEventData : IStringObjectEventData
    {
        public readonly RestartGameType type;

        public RestartEventData(RestartGameType type) =>
            this.type = type;

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(type), type.ToString("F") }
            };
    }

    public struct SetSettingEventData : IStringObjectEventData
    {
        public readonly bool status;

        public SetSettingEventData(bool status) =>
            this.status = status;

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(status), status }
            };
    }

    public struct SetLanguageEventData : IStringObjectEventData
    {
        public readonly SystemLanguage language;

        public SetLanguageEventData(SystemLanguage language) =>
            this.language = language;

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(SystemLanguage), language.ToString("F") }
            };
    }

    public struct FreeGemsEventData : IStringObjectEventData
    {
        public readonly int reward;

        public FreeGemsEventData(int reward) =>
            this.reward = reward;

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(reward), reward }
            };
    }

    public struct ItemEventData : IStringObjectEventData
    {
        public readonly string groupId;
        public readonly string itemId;

        public ItemEventData(string groupId, string itemId)
        {
            this.groupId = groupId;
            this.itemId = itemId;
        }

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(groupId), groupId },
                { nameof(itemId), itemId }
            };
    }

    public struct GDPREventData : IStringObjectEventData
    {
        public readonly string title;

        public GDPREventData(string title) =>
            this.title = title;

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(title), title }
            };
    }

    public readonly struct SocialEventData : IStringObjectEventData
    {
        public readonly string social;

        public SocialEventData(SocialParameters.Social socialType) =>
            social = socialType.ToString();

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(social), social }
            };
    }

    public readonly struct LanguageEventData : IStringObjectEventData
    {
        public readonly string language;

        public LanguageEventData(SystemLanguage systemLanguage) =>
            language = systemLanguage.ToString();

        public Dictionary<string, object> ToStringObjectData() =>
            new Dictionary<string, object>() {
                { nameof(language), language }
            };
    }
    
    #endregion // EVENT_DATA
}
