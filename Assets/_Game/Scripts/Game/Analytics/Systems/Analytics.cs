﻿using _Game.Scripts.Game.Analytics.Settings;

namespace _Game.Scripts.Game.Analytics.Systems
{
    interface IAnalytics
    {
        AnalyticsId id { get; }

        void Event(string eventName);
        void Event(string eventName, IEventData data);
        void Revenue(IRevenueData revenueData);
    }

    abstract class Analytics<TDataStructType, TRevenueStructType> : IAnalytics where TDataStructType : IEventData where TRevenueStructType : IRevenueData
    {
        public abstract AnalyticsId id { get; }

        public abstract void Event(string eventName);
        public abstract void Event(string eventName, TDataStructType data);
        public abstract void Revenue(TRevenueStructType revenueData);

        void IAnalytics.Event(string eventName, IEventData data) => Event(eventName, (TDataStructType)data);
        void IAnalytics.Revenue(IRevenueData revenueData) => Revenue((TRevenueStructType)revenueData);
    }
}
