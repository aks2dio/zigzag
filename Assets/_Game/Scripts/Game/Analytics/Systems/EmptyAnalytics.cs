﻿using _Game.Scripts.Game.Analytics.Settings;

namespace _Game.Scripts.Game.Analytics.Systems
{
    class EmptyAnalytics : IAnalytics
    {
        public AnalyticsId id => AnalyticsId.Empty;

        public void Event(string eventName) { }
        public void Event(string eventName, IEventData data) { }
        public void Revenue(IRevenueData revenueData) { }
    }
}
