﻿using _Game.Scripts.Game.Analytics.Settings;

namespace _Game.Scripts.Game.Analytics.Systems
{
    class AppMetricaAnalytics : Analytics<IStringObjectEventData, IYandexRevenueData>
    {
        public override AnalyticsId id => AnalyticsId.AppMetrica;

        public AppMetricaAnalytics()
        { }

        public override void Event(string eventName) =>
            AppMetrica.Instance.ReportEvent(eventName);

        public override void Event(string eventName, IStringObjectEventData eventData) =>
            AppMetrica.Instance.ReportEvent(eventName, eventData.ToStringObjectData());

        public override void Revenue(IYandexRevenueData revenueData) =>
            AppMetrica.Instance.ReportRevenue(revenueData.ToYandexRevenueData());
    }
}
