﻿using System.Collections.Generic;
using Core.Extensions;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace _Game.Scripts.Game.Input
{
    public class BackKeyListener : MyMonoBehaviour
    {
        [Inject] private ControlManager _ControlManager { get; }
        
        [SerializeField] private UnityEvent _OnEventHandled = new UnityEvent();
        
        
        private static readonly List<BackKeyListener> _list = new List<BackKeyListener>();
        
        public event UnityAction onEventHandled
        {
            add => _OnEventHandled.AddListener(value);
            remove => _OnEventHandled.RemoveListener(value);
        }
        
        ////////////////////////////////////////////

        private void OnEnable()
        {
            _list.Add(this);
            _ControlManager.OnBackKeyPress += OnBackKeyPress;
        }
        
        private void OnDisable()
        {
            _list.Remove(this);
            _ControlManager.OnBackKeyPress -= OnBackKeyPress;
        }
        
        ////////////////////////////////////////////

        private void OnBackKeyPress()
        {
            // слушатель должен быть последним в списке.
            // если эвент пойман, то минимум один в списке точно есть.
            if (_list[_list.Count - 1] != this)
                return;
            
            _OnEventHandled.Invoke();
            Log("Event handled");
        }
    }
}