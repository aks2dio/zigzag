﻿using Core.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Zenject;

namespace _Game.Scripts.Game.Input
{
    public class ControlManager : MyMonoBehaviour
    {
        [Inject] private ControlConfig _Config { get; }
        [Inject] private InputManager _InputManager { get; }
        
        [SerializeField] private UnityEvent _OnGameKeyPress = new UnityEvent();
        [SerializeField] private UnityEvent _OnBackKeyPress = new UnityEvent();
        
        public event UnityAction onGameKeyPress
        {
            add => _OnGameKeyPress.AddListener(value);
            remove => _OnGameKeyPress.RemoveListener(value);
        }
        public event UnityAction OnBackKeyPress
        {
            add => _OnBackKeyPress.AddListener(value);
            remove => _OnBackKeyPress.RemoveListener(value);
        }
        
        //////////////////////////////////////

        private void OnEnable()
        {
            _InputManager.onKeyDown += OnKeyDown;
            _InputManager.onTouchDown += OnTouchBegin;
            _InputManager.onMouseButtonDown += OnMouseButtonDown;
            
            _InputManager.AddListenedKeyCode(_Config.backKey);
            _InputManager.AddListenedKeyCode(_Config.changeDirectionKey);
        }

        private void OnDisable()
        {
            _InputManager.onKeyDown -= OnKeyDown;
            _InputManager.onTouchDown -= OnTouchBegin;
            _InputManager.onMouseButtonDown -= OnMouseButtonDown;
            
            _InputManager.RemoveListenedKeyCode(_Config.backKey);
            _InputManager.RemoveListenedKeyCode(_Config.changeDirectionKey);
        }
        
        //////////////////////////////////////

        private void OnMouseButtonDown(int button)
        {
            if (_Config.changeByClick == false)
                return;
            
            if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.currentSelectedGameObject != null) 
                return;
            
            GameKeyPressed();
        }

        private void OnTouchBegin(int touchId)
        {
            if (_Config.changeByClick || _Config.changeByTouch == false)
                return;
            
            if (EventSystem.current.IsPointerOverGameObject(touchId) ||
                EventSystem.current.currentSelectedGameObject != null)
                return;
            
            GameKeyPressed();
        }

        private void OnKeyDown(KeyCode keyCode)
        {
            if (keyCode == _Config.changeDirectionKey)
                GameKeyPressed();
            else if (keyCode == _Config.backKey)
                BackKeyPressed();
        }

        //////////////////////////////////////

        private void GameKeyPressed()
        {
            _OnGameKeyPress.Invoke();
            Log("GameKey pressed");
        }

        private void BackKeyPressed()
        {
            _OnBackKeyPress.Invoke();
            Log("BackKey pressed");
        }
    }
}