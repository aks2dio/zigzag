﻿using _Game.Scripts.Game.Analytics;
using _Game.Scripts.Game.Statistics;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Game.Social
{
    public class SocialButton : LoggedMonoBehaviour
    {
        [Inject] private StatisticsData _Statistics { get; }
        [Inject] private IAnalyticsManager _Analytics { get; }
        [Inject] private SocialParameters _Parameters { get; }

        [SerializeField] private SocialParameters.Social _Social;

        [ContextMenu("Click")]
        public void Click()
        {
            Log($"Clicked {_Social}");

            _Statistics.ViewSocial(_Social);
            _Analytics.ViewSocial(AnalyticsHelper.GetSocialEventData(_Social));

            Application.OpenURL(_Parameters.GetLink(_Social));
        }
    }
}
