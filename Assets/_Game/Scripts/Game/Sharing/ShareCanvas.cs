﻿using Core.Components;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Game.Sharing
{
    class ShareCanvas : MyMonoBehaviour
    {
        [Header("Text Components")]
        [SerializeField] private UniversalText _Text = null;

        [Header("Graphics Components")]
        [SerializeField] private Image _Image = null;
        [SerializeField] private Image _Background = null;

        private Sprite _loadedImage;

        public bool isActive => gameObject.activeSelf;

        /////////////////////////////////////////////////////////////////////

        protected void OnDisable() => Deactivate();

        protected override void OnChecking()
        {
            CheckField(_Text);
            CheckField(_Image);
            CheckField(_Background);
        }

        /////////////////////////////////////////////////////////////////////

        public void Activate() =>
            SetActive(true);

        public void Deactivate()
        {
            SetActive(false);

            SetText("");
            SetImage("");
            SetBackgroundColor(Color.white);
            Resources.UnloadAsset(_loadedImage);
        }

        public void SetText(string text) => _Text.SetText(text);

        public void SetImage(string imagePath)
        {
            _loadedImage = !string.IsNullOrEmpty(imagePath) ? Resources.Load<Sprite>(imagePath) : null;
            _Image.sprite = _loadedImage;
        }

        public void SetBackgroundColor(Color color)
        {
            _Background.color = color;
        }

        /////////////////////////////////////////////////////////////////////

        private void SetActive(bool status)
        {
            if (isActive == status)
                return;

            gameObject.SetActive(status);
        }
    }
}
