﻿
namespace _Game.Scripts.Parameters.Interfaces
{
    public interface IDistanceScoreParameters
    {
        int scorePerStep { get; }
    }
}
