﻿using _Game.Scripts.Parameters.Classes;
using UnityEngine;

namespace _Game.Scripts.Parameters.Interfaces
{
    public interface IGemManagerParameters
    {
        int platformsInBlock { get; }
        PlacementType placementType { get; }
        float squareSideOfGenerationArea { get; }
        Vector3 positionOffsetMask { get; }
    }
}
