﻿
namespace _Game.Scripts.Parameters.Interfaces
{
    public interface IPlatformParameters
    {
        float size { get; }
        float thickness { get; }
    }
}
