﻿using UnityEngine;

namespace _Game.Scripts.Parameters.Interfaces
{
    interface IGamePrefabs
    {
        GameObject ball { get; }
        GameObject gem { get; }
        GameObject platform { get; }
    }
}
