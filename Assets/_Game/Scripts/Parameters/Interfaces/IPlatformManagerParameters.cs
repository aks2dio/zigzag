﻿using UnityEngine;

namespace _Game.Scripts.Parameters.Interfaces
{
    public interface IPlatformManagerParameters
    {
        AnimationCurve probabilityChangeDirection { get; }
        int stepThresholdBack { get; }
        int stepThresholdForward { get; }
        int initialStepsOneDirection { get; }
        int initialAreaSize { get; }
        int initialPrecalculatedSteps { get; }
    }
}
