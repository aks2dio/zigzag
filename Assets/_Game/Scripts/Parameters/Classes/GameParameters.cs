﻿using _Game.Scripts.Game;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [CreateAssetMenu(fileName = nameof(GameParameters), menuName = DefinitionsHelper.Parameters + nameof(GameParameters))]
    public class GameParameters : ScriptableObject, IGameParameters
    {
        #region SERIALIZE_FIELDS
        [SerializeField] private BallParameters _Ball = null;
        [SerializeField] private PlatformParameters _Platform = null;

        [SerializeField] private DirectionParameters _Direction = null;

        [SerializeField] private PlatformManagerParameters _PlatformManager = null;
        [SerializeField] private GemManagerParameters _GemManager = null;

        [SerializeField] private GemScoreParameters _GemScore = null;
        [SerializeField] private TurnsScoreParameters _TurnsScore = null;
        [SerializeField] private DistanceScoreParameters _DistanceScore = null;
        #endregion // SERIALIZE_FIELDS

        #region PUBLIC_VALUES
        public IBallParameters Ball => _Ball;
        public IPlatformParameters Platform => _Platform;
        public IDirectionParameters Direction => _Direction;
        public IPlatformManagerParameters PlatformManager => _PlatformManager;
        public IGemManagerParameters GemManager => _GemManager;
        public IGemScoreParameters GemScore => _GemScore;
        public ITurnsScoreParameters TurnsScore => _TurnsScore;
        public IDistanceScoreParameters DistanceScore => _DistanceScore;
        #endregion // PUBLIC_VALUES
    }
}