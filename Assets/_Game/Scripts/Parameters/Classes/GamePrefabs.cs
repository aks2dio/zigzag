﻿using _Game.Scripts.Game;
using _Game.Scripts.Parameters.Interfaces;
using UnityEngine;

namespace _Game.Scripts.Parameters.Classes
{
    [CreateAssetMenu(fileName = nameof(GamePrefabs), menuName = DefinitionsHelper.Parameters + nameof(GamePrefabs))]
    class GamePrefabs : ScriptableObject, IGamePrefabs
    {
        #region SERIALIZE_FIELDS
        [SerializeField] private GameObject _Ball = null;
        [SerializeField] private GameObject _Gem = null;
        [SerializeField] private GameObject _Platform = null;
        #endregion // SERIALIZE_FIELDS

        #region PUBLIC_VALUES
        public GameObject ball => _Ball;
        public GameObject gem => _Gem;
        public GameObject platform => _Platform;
        #endregion // PUBLIC_VALUES
    }
}
