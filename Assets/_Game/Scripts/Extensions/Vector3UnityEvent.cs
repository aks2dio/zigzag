﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Extensions
{
    [Serializable]
    class Vector3UnityEvent : UnityEvent<Vector3>
    {
    }
}
