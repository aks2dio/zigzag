﻿    namespace _Game.Scripts.Extensions
    {
        static class IntExtensions
        {
            public static bool IsInInterval(this int value, int minBorder, int maxBorder)
                => minBorder <= value && value <= maxBorder;
        }
    }
