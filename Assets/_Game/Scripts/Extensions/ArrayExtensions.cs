﻿using System;

namespace _Game.Scripts.Extensions
{
    static class ArrayExtensions
    {
        public static void ForEach<T>(this T[] array, Action<T> action)
        {
            if (array == null)
                return;

            for(var i = 0; i < array.Length; i++)
                action?.Invoke(array[i]);
        }
    }
}
