﻿using System;
using _Game.Scripts.Game.Objects.Platform;
using UnityEngine.Events;

namespace _Game.Scripts.Extensions
{
    [Serializable]
    public class IPlatformUnityEvent : UnityEvent<IPlatform>
    {
    }
}
