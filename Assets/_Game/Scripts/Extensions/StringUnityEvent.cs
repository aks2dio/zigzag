﻿using System;
using UnityEngine.Events;

namespace _Game.Scripts.Extensions
{
    [Serializable]
    class StringUnityEvent : UnityEvent<string>
    { }
}
