﻿using _Game.Scripts.SoundManagement.Collection;
using _Game.Scripts.SoundManagement.Library;
using _Game.Scripts.SoundManagement.Sourcing;
using Core.Extensions;
using UnityEngine;
using UnityEngine.Audio;

namespace _Game.Scripts.SoundManagement
{
    enum SnapshotType
    {
        Normal,
        Menu
    }
    
    class SoundManager : MyMonoBehaviour, ISoundManager
    {
        [Header("Parameters")]
        [SerializeField] private SoundManagerSettings _Settings = null;

        [Header("Attachments")]
        [SerializeField] private SoundLibrary _Library = null;
        [SerializeField] private Sources _Sources = null;
        [SerializeField] private AudioMixer _AudioMixer = null;

        [Header("Snapshots")] 
        [SerializeField] private float _SnapshotsTransition = 0f;
        [SerializeField] private AudioMixerSnapshot _NormalSnapshot = null;
        [SerializeField] private AudioMixerSnapshot _MenuSnapshot = null;

        private IPlayedSoundsCollection _collection;

        public ISoundLibrary library => _Library;
        public ISources sources => _Sources;

        ///////////////////////////////////////////////

        private void Awake()
        {
            _collection = new PlayedSoundsCollection(this, _Settings.minOffsetBetweenSameSounds, 
                _Settings.maxDifferenetSoundsAtOneTime, _Settings.maxSameSoundsAtOneTime);
        }

        protected override void OnChecking()
        {
            CheckField(_Library);
            CheckField(_Settings);
            CheckField(_AudioMixer);
        }

        ///////////////////////////////////////////////

        public void Play(AudioSource source, AudioClip clip)
            => Play(source, clip, false);

        public void PlayLooped(AudioSource source, AudioClip clip)
            => Play(source, clip, true);

        public void Stop(AudioSource source)
        {
            if (source == null) {
                Error("Empty source!");
                return;
            }

            source.Stop();
            _collection.RemovePlayed(source.clip);
            Log($"Stopped audioSource {source.name}");
        }

        public void SetEnable(AudioSource source, bool status)
        {
            if (source == null) {
                Error("Empty source!");
                return;
            }

            if (status == !source.mute)
                return;

            source.mute = !status;
            Log($"{( status ? "Enabled" : "Disabled" )} source {source.name}");
        }

        public void SelectNormalSnapshot() => SelectSnapshot(SnapshotType.Normal);
        public void SelectMenuSnapshot() => SelectSnapshot(SnapshotType.Menu);
        
        public void SelectSnapshot(SnapshotType snapshotType)
        {
            switch (snapshotType)
            {
                case SnapshotType.Menu: _MenuSnapshot.TransitionTo(_SnapshotsTransition);
                    break;
                case SnapshotType.Normal: _NormalSnapshot.TransitionTo(_SnapshotsTransition);
                    break;
                default: Error("Wrong snapshot!"); return;
            }
            Log($"Snapshot: {snapshotType:F}");
        }

        ///////////////////////////////////////////////

        private void Play(AudioSource source, AudioClip clip, bool looped)
        {
            if (source == null) {
                Error("Empty source!");
                return;
            }

            if (clip == null) {
                Error("Empty clip");
                return;
            }

            if (_collection == null) {
                Error("Null collection");
                return;
            }

            if (_collection.CanBePlayed(clip) == false)
                return;

            if (looped) {
                source.loop = true;
                source.clip = clip;
                source.Play();
            }
            else {
                source.PlayOneShot(clip);
                _collection.AddPlayed(clip);
            }

            Log($"Play {( looped ? "looped" : "" )}sound {clip.name}");
        }
    }
}
