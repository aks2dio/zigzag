﻿using _Game.Scripts.Game.Data;
using _Game.Scripts.SoundManagement.InGameComponents.Base;
using Zenject;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class EnabledSyncer : SoundsComponent
    {
        private SettingsData _settingsData { get; set; }

        ////////////////////////////////////////////////////////////

        [Inject]
        public void Construct(SettingsData settingsData)
        {
            _settingsData = settingsData;

            _settingsData.onSoundChanged += OnSoundChanged;
            _settingsData.onMusicChanged += OnMusicChanged;

            UpdateMusicEnabling();
            UpdateSoundEnabling();
        }

        private void OnDestroy()
        {
            if (_settingsData == null)
                return;

            _settingsData.onSoundChanged -= OnSoundChanged;
            _settingsData.onMusicChanged -= OnMusicChanged;
        }

        ////////////////////////////////////////////////////////////

        protected override void OnSetSoundManager()
        {
            UpdateMusicEnabling();
            UpdateSoundEnabling();
        }

        private void OnSoundChanged()
            => UpdateSoundEnabling();

        private void OnMusicChanged()
            => UpdateMusicEnabling();

        ////////////////////////////////////////////////////////////

        private void UpdateSoundEnabling()
        {
            if (CanChangeEnabling() == false)
                return;

            var soundEnabled = _settingsData.soundOn;
            _soundManager.SetEnable(_soundManager.sources.fx, soundEnabled);
            _soundManager.SetEnable(_soundManager.sources.ui, soundEnabled);
        }

        private void UpdateMusicEnabling()
        {
            if (CanChangeEnabling() == false)
                return;

            _soundManager.SetEnable(_soundManager.sources.music, _settingsData.musicOn);
        }

        private bool CanChangeEnabling()
            => _soundManager != null && _settingsData != null;
    }
}
