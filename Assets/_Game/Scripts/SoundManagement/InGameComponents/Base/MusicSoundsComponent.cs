﻿using _Game.Scripts.SoundManagement.Library;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.InGameComponents.Base
{
    class MusicSoundsComponent : SoundsComponent
    {
        protected IMusicLibrary _library => _soundManager?.library?.music;
        protected AudioSource _source => _soundManager?.sources?.music;
    }
}
