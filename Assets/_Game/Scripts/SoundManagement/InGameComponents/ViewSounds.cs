﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class ViewSounds : UISoundsComponent
    {
        public void PlayOpen()
            => _soundManager?.Play(_source, _library?.viewOpened);

        public void PlayClose()
            => _soundManager?.Play(_source, _library?.viewClosed);
    }
}
