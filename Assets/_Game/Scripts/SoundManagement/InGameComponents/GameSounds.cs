﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class GameSounds : FXSoundsComponent
    {
        public void PlayGameStart()
            => _soundManager?.Play(_source, _library?.gameStart);

        public void PlayGameOver()
            => _soundManager?.Play(_source, _library?.gameOver);
    }
}
