﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class ButtonSounds : UISoundsComponent
    {
        public void PlayClick()
            => _soundManager?.Play(_source, _library?.buttonClick);
    }
}
