﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class BallSounds : FXSoundsComponent
    {
        public void PlayChangeDirection()
            => _soundManager?.Play(_source, _library?.changeDirection);
    }
}
