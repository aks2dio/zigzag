﻿using _Game.Scripts.SoundManagement.InGameComponents.Base;

namespace _Game.Scripts.SoundManagement.InGameComponents
{
    class PlatformSounds : FXSoundsComponent
    {
        public void PlayCrack()
            => _soundManager?.Play(_source, _library?.platformCrack);
    }
}
