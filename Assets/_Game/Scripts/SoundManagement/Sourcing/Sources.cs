﻿using System;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.Sourcing
{
    [Serializable]
    class Sources : ISources
    {
        [SerializeField] private AudioSource _Music = null;
        [SerializeField] private AudioSource _FX = null;
        [SerializeField] private AudioSource _UI = null;

        public AudioSource music => _Music;
        public AudioSource fx => _FX;
        public AudioSource ui => _UI;
    }
}
