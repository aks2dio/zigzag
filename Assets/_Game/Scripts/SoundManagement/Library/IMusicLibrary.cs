﻿using UnityEngine;

namespace _Game.Scripts.SoundManagement.Library
{
    interface IMusicLibrary
    {
        AudioClip[] musicClips { get; }

        AudioClip randomMusic { get; }
    }
}
