﻿using _Game.Scripts.Game;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.Library
{
    [CreateAssetMenu(fileName = nameof(SoundLibrary), menuName = DefinitionsHelper.Sound + nameof(SoundLibrary))]
    class SoundLibrary : ScriptableObject, ISoundLibrary
    {
        [SerializeField] private MusicLibrary _MusicLibrary = null;
        [SerializeField] private FXLibrary _FXLibrary = null;
        [SerializeField] private UILibrary _UILibrary = null;

        public IMusicLibrary music => _MusicLibrary;
        public IFXLibrary fx => _FXLibrary;
        public IUILibrary ui => _UILibrary;
    }
}
