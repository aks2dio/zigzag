﻿namespace _Game.Scripts.SoundManagement.Library
{
    interface ISoundLibrary
    {
        IMusicLibrary music { get; }
        IFXLibrary fx { get; }
        IUILibrary ui { get; }
    }
}
