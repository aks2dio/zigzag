﻿using _Game.Scripts.Game;
using UnityEngine;

namespace _Game.Scripts.SoundManagement.Library
{
    [CreateAssetMenu(fileName = nameof(FXLibrary), menuName = DefinitionsHelper.Sound + nameof(FXLibrary))]
    class FXLibrary : ScriptableObject, IFXLibrary
    {
        [SerializeField] private AudioClip _GameStart = null;
        [SerializeField] private AudioClip _GameOver = null;
        [SerializeField] private AudioClip _ChangeDirection = null;
        [SerializeField] private AudioClip _CollectGem = null;
        [SerializeField] private AudioClip _PlatformCrack = null;

        public AudioClip gameStart => _GameStart;
        public AudioClip gameOver => _GameOver;
        public AudioClip changeDirection => _ChangeDirection;
        public AudioClip collectGem => _CollectGem;
        public AudioClip platformCrack => _PlatformCrack;
    }
}
