﻿using _Game.Scripts.Game;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine;

namespace _Game.Scripts.GooglePlay.Parameters
{

    [CreateAssetMenu(fileName = nameof(GooglePlayServiceSettings), menuName = DefinitionsHelper.Parameters + nameof(GooglePlayServiceSettings))]
    class GooglePlayServiceSettings : ScriptableObject
    {
        [Header("Leaderboard")]
        [SerializeField] private string _LeaderboardId = "";

        [Space] [Header("Cloud Save")]
        [SerializeField] private string _SaveName = "";
        [SerializeField] private DataSource _DataSource = DataSource.ReadCacheOrNetwork;
        [SerializeField] private ConflictResolutionStrategy _ConflictResolutionStrategy = ConflictResolutionStrategy.UseMostRecentlySaved;

        public string leaderboardId => _LeaderboardId;
        public string saveName => _SaveName;
        public DataSource dataSource => _DataSource;
        public ConflictResolutionStrategy conflictResolutionStrategy => _ConflictResolutionStrategy;
    }
}
