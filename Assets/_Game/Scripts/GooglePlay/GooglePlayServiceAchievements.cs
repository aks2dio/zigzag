﻿using Core.Extensions;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.GooglePlay
{
    [RequireComponent(typeof(GooglePlayServiceManager))]
    public class GooglePlayServiceAchievements : MyMonoBehaviour
    {
        [Inject] private GooglePlayServiceManager _manager { get; }
        
        //////////////////////////////////////////////////////////////

        [ContextMenu("Show Achievements")]
        public async void ShowAchievments()
        {
            if (_manager.isAuthenticated == false)
                await _manager.Login();
            
            if (_manager.isAuthenticated == false)
            {
                Error("Isn't authenticated to show Achievements!");
                return;
            }

            Log("Show Achievements UI");
            _manager.playGamesPlatform.ShowAchievementsUI();
        }
        
        //////////////////////////////////////////////////////////////

        public void ReportAchievementProgress(string achievementId, double progress)
        {
            if (_manager.isAuthenticated == false)
                return;
            
            _manager.playGamesPlatform.ReportProgress(achievementId, progress, 
                success => Log($"Achievement report: {success}"));
        }

        public void CompleteAchievement(string achievementId)
        {
            if (_manager.isAuthenticated == false)
                return;
            
            _manager.playGamesPlatform.ReportProgress(achievementId, 100f, 
                success => Log($"Achievement complete: {success}"));
        }
    }
}