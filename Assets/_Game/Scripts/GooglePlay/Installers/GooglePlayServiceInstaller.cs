﻿using _Game.Scripts.GooglePlay.Parameters;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.GooglePlay.Installers
{
    class GooglePlayServiceInstaller : MonoInstaller
    {
        [SerializeField] private GooglePlayServiceSettings _Settings = null;
        [SerializeField] private GooglePlayServiceLeaderboard _Leaderboard = null;
        [SerializeField] private GooglePlayServiceCloudSaving _CloudSaving = null;
        [SerializeField] private GooglePlayServiceAchievements _Achievements = null;
        
        public override void InstallBindings()
        {
            Container.Bind<GooglePlayServiceSettings>().FromInstance(_Settings).AsSingle();
            Container.Bind<GooglePlayServiceLeaderboard>().FromInstance(_Leaderboard).AsSingle();
            Container.Bind<GooglePlayServiceCloudSaving>().FromInstance(_CloudSaving).AsSingle();
            Container.Bind<GooglePlayServiceAchievements>().FromInstance(_Achievements).AsSingle();
        }
    }
}