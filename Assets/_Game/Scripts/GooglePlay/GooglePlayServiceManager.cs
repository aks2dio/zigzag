﻿using System.Threading.Tasks;
using Core.Extensions;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace _Game.Scripts.GooglePlay
{
    public class GooglePlayServiceManager : MyMonoBehaviour
    {
        [Inject] public GooglePlayServiceAchievements achievements { get; }
        [Inject] public GooglePlayServiceLeaderboard leaderboard { get; }
        [Inject] public GooglePlayServiceCloudSaving cloudSaving { get; }

        [Header("Events")]
        [SerializeField] private UnityEvent _OnAuthentificateStatusChanged = new UnityEvent();

        private bool _isAuthenticating;
        
        public PlayGamesPlatform playGamesPlatform { get; private set; }
        public bool isAuthenticated => playGamesPlatform != null && playGamesPlatform.IsAuthenticated();
        
        public event UnityAction onAuthentificateStatusChanged
        {
            add => _OnAuthentificateStatusChanged.AddListener(value);
            remove => _OnAuthentificateStatusChanged.RemoveListener(value);
        }

        ////////////////////////////////////////////////

        private void Start() => 
            _ = Login(SignInInteractivity.CanPromptOnce);

        ////////////////////////////////////////////////

        #region LOGIN

        public async Task LoginOrLogout()
        {
            if (isAuthenticated)
                await Logout();
            else
                await Login();
        }

        public async Task Login() => 
            await Login(SignInInteractivity.CanPromptAlways);

        public async Task Logout()
        {
            if (isAuthenticated == false)
                return;

            if (playGamesPlatform == null)
                playGamesPlatform = CreatePlayGamesPlatform();

            playGamesPlatform.SignOut();
            Log("Sign out");

            while (isAuthenticated)
                await Task.Delay(100);

            await Task.Yield();
            _OnAuthentificateStatusChanged?.Invoke();
        }

        private async Task Login(SignInInteractivity promptType)
        {
            if (isAuthenticated)
                return;

            if (playGamesPlatform == null)
                playGamesPlatform = CreatePlayGamesPlatform();

            _isAuthenticating = true;
            playGamesPlatform.Authenticate(promptType, OnAuthentificate);

            while (_isAuthenticating)
                await Task.Delay(100);

            await Task.Yield();
            cloudSaving.LoadGame();
            _OnAuthentificateStatusChanged?.Invoke();
        }

        private void OnAuthentificate(SignInStatus status)
        {
            _isAuthenticating = false;
            _OnAuthentificateStatusChanged?.Invoke();
            
            Log(status.ToString("F"));
        }
        
        #endregion // LOGIN

        #region CONFIGURATION

        private PlayGamesPlatform CreatePlayGamesPlatform()
        {
            PlayGamesPlatform.InitializeInstance(CreateConfiguration());
            PlayGamesPlatform.DebugLogEnabled = true;
            Log("Create Instance");
            
            return PlayGamesPlatform.Activate();
        }

        private PlayGamesClientConfiguration CreateConfiguration() =>
            new PlayGamesClientConfiguration.Builder()
                //.RequestServerAuthCode(false)
                //.AddOauthScope("profile")
                //.RequestIdToken()
                .EnableSavedGames()
                .Build();

        #endregion // CONFIGURATION
    }
}
