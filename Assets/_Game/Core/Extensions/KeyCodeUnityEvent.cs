﻿using System;

namespace UnityEngine.Events
{
    [Serializable]
    public class KeyCodeUnityEvent : UnityEvent<KeyCode>
    {
        public KeyCodeUnityEvent() : base() { }
    }
}