﻿namespace Core.Modules
{
    public interface IVibrator
    {
        void Vibrate(long milliseconds = 250);
        void Stop();
    }
}