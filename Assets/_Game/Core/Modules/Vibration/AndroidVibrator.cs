﻿using UnityEngine;

namespace Core.Modules
{
    internal class AndroidVibrator : LoggedClass, IVibrator
    {
        private AndroidJavaClass _unityPlayer;
        private AndroidJavaObject _currentActivity;
        private AndroidJavaObject _vibrator;

        ////////////////////////////////////////////
        
        public AndroidVibrator()
        {
            _unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            _currentActivity = _unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            _vibrator = _currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
        }

        ////////////////////////////////////////////

        public void Vibrate(long milliseconds = 250)
        {
            _vibrator.Call("vibrate", milliseconds);
            Log($"Vibrate: {milliseconds}");
        }

        public void Stop()
        {
            _vibrator.Call("cancel");
            Log("Stop");
        }
    }
}