﻿namespace Core.Systems.Reset
{
    public interface IResetable
    {
        void Reset();
    }
}
