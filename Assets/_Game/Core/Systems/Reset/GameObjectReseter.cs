﻿using UnityEngine;

namespace Core.Systems.Reset
{
    public class GameObjectReseter : MonoBehaviour
    {
        public void Reset()
        {
            foreach (var component in GetComponents<IResetable>())
                component.Reset();
        }
    }
}
