﻿
namespace Core.Components.UI.Screens
{
    public enum ScreenType
    {
        None = 0,
        Start = 1,
        Game = 2,
        Pause = 3,
        GameOver = 4,
        Settings = 5,
        IAPShop = 6,
        SkinsShop = 7,
        Leaderboard = 8,
        Achievements = 9,
        Rate = 10,
        Info = 11,
        TextShower = 12,
        Reward = 13
    }
}
