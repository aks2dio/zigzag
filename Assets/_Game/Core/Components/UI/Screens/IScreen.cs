﻿using Core.Interfaces;
using UnityEngine.Events;

namespace Core.Components.UI.Screens
{
    public interface IScreen : IGameObjectHost
    {
        ScreenType type { get; }
        bool countedAsFirst { get; }
        bool countedAsLast { get; }
        bool canBeHided { get; }
        bool opened { get; }
        bool hided { get; }

        UnityEvent onOpened { get; set; }
        UnityEvent onHided { get; set; }
        UnityEvent onClosed { get; set; }

        void Open();
        void Hide();
        void Close();
    }
}
