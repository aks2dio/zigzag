﻿namespace UnityEngine.UI
{
    public class ClickableElement : MonoBehaviour
    {
        [ContextMenu("Click")]
        public void Click()
        {
            if (TryGetComponent<Button>(out var button))
                button.onClick.Invoke();
        }
    }
}