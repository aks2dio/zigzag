Изначально это было тестовым заданием. Со временем это переросло в полноценный проект, опубликованный в Google Play.  
**Ссылка на приложение в магазине:** https://play.google.com/store/apps/details?id=com.ask2dio.twisttwast&hl=ru&gl=US

#### Стек:
- Unity  
- URP  
- Addressables  
- Cinemachine  
- TextMeshPro  
- DOTween  
- Zenject  
- Unity Ads  
- AdMob  
- Unity IAP  
- Unity Analytics  
- AppMetrica  
- Google Play Services  
- Unity Notifications  

Используется кастомные системы UI, аудио и локализации.  

#### Из интересного реализовано:
- реклама  
- покупки (Cons & NonCons)  
- нотификации  
- кастомные toast  
- виброотклик  
- магазин скинов  
- игровая статистика  
- достижения  
- топ игроков  
- авторизация через Google  
- облачные сохранения  
- переход по ссылкам в соц сети  
- возможность поделиться результатами игры с моментальным скриншотом  

**Входная точка:** GameManager
Все настройки непосредственно игры хранятся в папке *ScriptableObjects*.
